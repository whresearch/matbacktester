classdef BacktestResult < handle
    %BACKTESTRESULT Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Param
        Strategy
        OrderBook
        TradeBook
        ResultStats
    end
    
    methods (Access = protected)
        
        function AnalyseResult( this )
            % calcualte the backtest results analysis
            
            if nargin ~= 1
                error('1 arguments required'); 
            end
            
            Amount = this.TradeBook.Amount;
            Amount(~this.TradeBook.IsLong) = Amount(~this.TradeBook.IsLong) * -1;
            
            if sum(Amount) ~= 0
                warning('unclosed tradebook'); 
            end
            
            % analyze orders
            NbLmtLong = 0;
            NbLmtShort = 0;
            NbLmtLongCancel = 0;
            NbLmtShortCancel = 0;
            AmtLmtLong = 0;
            AmtLmtShort = 0;
            AmtLmtLongFill = 0;
            AmtLmtShortFill = 0;
            AvgSec2FillLong = 0;
            AvgSec2FillShort = 0;
            
            for i = 1 : height(this.OrderBook)
                Order = this.OrderBook(i, :);
                if ~Order.IsIOC && ~isnan(Order.Price)
                    if Order.IsLong
                        NbLmtLong = NbLmtLong + 1;
                        if Order.Amount > 0
                            NbLmtLongCancel = NbLmtLongCancel + 1;
                        end
                        AmtLmtLong = AmtLmtLong + Order.Amount + Order.FilledAmount;
                        AmtLmtLongFill = AmtLmtLongFill + Order.FilledAmount;
                        if ~isempty(Order.PartialFilledAmount{1})
                            AvgSec2FillLong = AvgSec2FillLong + ...
                                sum(Order.PartialFilledAmount{1} .* seconds(Order.PartialFilledTime{1} - Order.SubmitTime));
                        end
                    else
                        NbLmtShort = NbLmtShort + 1;
                        if Order.Amount > 0
                            NbLmtShortCancel = NbLmtShortCancel + 1;
                        end
                        AmtLmtShort = AmtLmtShort + Order.Amount + Order.FilledAmount;
                        AmtLmtShortFill = AmtLmtShortFill + Order.FilledAmount;
                        if ~isempty(Order.PartialFilledAmount{1})
                            AvgSec2FillShort = AvgSec2FillShort + ...
                                sum(Order.PartialFilledAmount{1} .* seconds(Order.PartialFilledTime{1} - Order.SubmitTime));
                        end
                    end
                end
            end
            
            NbLmt = NbLmtLong + NbLmtShort;
            NbLmtCancel = NbLmtLongCancel + NbLmtShortCancel;
            AmtLmt = AmtLmtLong + AmtLmtShort;
            AmtLmtFill = AmtLmtLongFill + AmtLmtShortFill;
            AmtLmtCancel = AmtLmt - AmtLmtFill;
            AmtLmtLongCancel = AmtLmtLong - AmtLmtLongFill;
            AmtLmtShortCancel = AmtLmtShort - AmtLmtShortFill;
            AvgSec2Fill = (AvgSec2FillLong + AvgSec2FillShort) / AmtLmtFill;
            AvgSec2FillLong = AvgSec2FillLong / AmtLmtLongFill;
            AvgSec2FillShort = AvgSec2FillShort / AmtLmtShortFill;
            FillRate = AmtLmtFill / AmtLmt;
            FillRateLong = AmtLmtLongFill / AmtLmtLong;
            FillRateShort = AmtLmtShortFill / AmtLmtShort;
            
            % analyze trades
            PnLMoneyLong = 0;
            PnLMoneyShort = 0;
            PnLPriceLong = 0;
            PnLPriceShort = 0;
            AmtTradeLong = 0;
            AmtTradeShort = 0;
            WinRateLong = 0;
            WinRateShort = 0;
            MaxAmtTradeLong = 0;
            MaxAmtTradeShort = 0;
            CommTradeLong = 0;
            CommTradeShort = 0;
            
            PosLong = 0;
            PosShort = 0;
            
            for i = 1 : height(this.TradeBook)
                Trade = this.TradeBook(i, :);
                if Trade.IsOpen
                    if Trade.IsLong
                        PnLPriceLong = PnLPriceLong + Trade.PnLPrice;
                        PnLMoneyLong = PnLMoneyLong + Trade.PnLMoney - Trade.Commission;
                        CommTradeLong = CommTradeLong + Trade.Commission;
                        PosLong = PosLong + Trade.Amount;
                        MaxAmtTradeLong = max(MaxAmtTradeLong, PosLong);
                    else
                        PnLPriceShort = PnLPriceShort + Trade.PnLPrice;
                        PnLMoneyShort = PnLMoneyShort + Trade.PnLMoney - Trade.Commission;
                        CommTradeShort = CommTradeShort + Trade.Commission;
                        PosShort = PosShort + Trade.Amount;
                        MaxAmtTradeShort = max(MaxAmtTradeShort, PosShort);
                    end
                else
                    if Trade.IsLong
                        PnLPriceShort = PnLPriceShort + Trade.PnLPrice;
                        PnLMoneyShort = PnLMoneyShort + Trade.PnLMoney - Trade.Commission;
                        CommTradeShort = CommTradeShort + Trade.Commission;
                        AmtTradeShort = AmtTradeShort + Trade.Amount;
                        if Trade.PnLPrice > 0
                            WinRateShort = WinRateShort + Trade.Amount;
                        end
                        PosShort = PosShort - Trade.Amount;
                    else
                        PnLPriceLong = PnLPriceLong + Trade.PnLPrice;
                        PnLMoneyLong = PnLMoneyLong + Trade.PnLMoney - Trade.Commission;
                        CommTradeLong = CommTradeLong + Trade.Commission;
                        AmtTradeLong = AmtTradeLong + Trade.Amount;
                        if Trade.PnLPrice > 0
                            WinRateLong = WinRateLong + Trade.Amount;
                        end
                        PosLong = PosLong - Trade.Amount;
                    end
                end
            end
            
            PnLMoney = PnLMoneyLong + PnLMoneyShort;
            PnLPrice = PnLPriceLong + PnLPriceShort;
            CommTrade = CommTradeLong + CommTradeShort;
            AmtTrade = AmtTradeLong + AmtTradeShort;
            MaxAmtTrade = max(MaxAmtTradeLong, MaxAmtTradeShort);
            WinRate = (WinRateLong + WinRateShort) / AmtTrade;
            WinRateLong = WinRateLong / AmtTradeLong;
            WinRateShort = WinRateShort / AmtTradeShort;
            AvgPnLMoney = PnLMoney / AmtTrade;
            AvgPnLMoneyLong = PnLMoneyLong / AmtTradeLong;
            AvgPnLMoneyShort = PnLMoneyShort / AmtTradeShort;
            AvgPnLPrice = PnLPrice / AmtTrade;
            AvgPnLPriceLong = PnLPriceLong / AmtTradeLong;
            AvgPnLPriceShort = PnLPriceShort / AmtTradeShort;
            
            % build table
            this.ResultStats = table( ...
                AmtTrade, AmtTradeLong, AmtTradeShort, ...
                PnLMoney, PnLMoneyLong, PnLMoneyShort, ...
                PnLPrice, PnLPriceLong, PnLPriceShort, ...
                CommTrade, CommTradeLong, CommTradeShort, ...
                AvgPnLMoney, AvgPnLMoneyLong, AvgPnLMoneyShort, ...
                AvgPnLPrice, AvgPnLPriceLong, AvgPnLPriceShort, ...
                MaxAmtTrade, MaxAmtTradeLong, MaxAmtTradeShort, ...
                WinRate, WinRateLong, WinRateShort, ...
                NbLmt, NbLmtLong, NbLmtShort, ...
                NbLmtCancel, NbLmtLongCancel, NbLmtShortCancel, ...
                AmtLmt, AmtLmtLong, AmtLmtShort, ...
                AmtLmtFill, AmtLmtLongFill, AmtLmtShortFill, ...
                AmtLmtCancel, AmtLmtLongCancel, AmtLmtShortCancel, ...
                FillRate, FillRateLong, FillRateShort, ...
                AvgSec2Fill, AvgSec2FillLong, AvgSec2FillShort);
        end
        
        function CreateOrderBook( this, Orders )
            
            Length = length(Orders);
            
            InstrumentID = cell(Length, 1);
            SubmitTime = repmat(datetime, Length, 1);
            IsOpen = false(Length, 1);
            IsLong = false(Length, 1);
            IsIOC = false(Length, 1);
            Price = NaN(Length, 1);
            Amount = NaN(Length, 1);
            FilledAmount = NaN(Length, 1);
            CancelledAmount = NaN(Length, 1);
            PartialFilledTime = cell(Length, 1);
            PartialFilledAmount = cell(Length, 1);
            IsCancelled = false(Length, 1);
            Status = cell(Length, 1);
            LastTime = repmat(datetime, Length, 1);
            
            for i = 1 : Length
                InstrumentID{i} = this.Param.Instrument{Orders(i).InstrumentID};
                SubmitTime(i) = Orders(i).SubmitTime;
                IsOpen(i) = Orders(i).IsOpen;
                IsLong(i) = Orders(i).IsLong;
                IsIOC(i) = Orders(i).IsIOC;
                Price(i) = Orders(i).Price;
                Amount(i) = Orders(i).Amount;
                FilledAmount(i) = Orders(i).FilledAmount;
                CancelledAmount(i) = Amount(i) - FilledAmount(i);
                PartialFilledTime{i} = Orders(i).PartialFilledTime;
                PartialFilledAmount{i} = Orders(i).PartialFilledAmount;
                IsCancelled(i) = Orders(i).Status == OrderStatus.Cancel;
                Status{i} = char(Orders(i).Status);
                LastTime(i) = Orders(i).LastTime;
            end
            
            SubmitTime.Format = 'yyyy-MM-dd hh:mm:ss.SSSS';
            LastTime.Format = 'yyyy-MM-dd hh:mm:ss.SSSS';
            
            this.OrderBook = table(InstrumentID, SubmitTime, IsOpen, IsLong, ...
                IsIOC, Price, Amount, FilledAmount, CancelledAmount, PartialFilledTime, ...
                PartialFilledAmount, IsCancelled, Status, LastTime);
            
        end
        
        function CreateTradeBook( this, Trades )
            
            Length = length(Trades);
            
            InstrumentID = cell(Length, 1);
            IsOpen = false(Length, 1);
            IsLong = false(Length, 1);
            Time = repmat(datetime, Length, 1);
            Amount = NaN(Length, 1);
            Price = NaN(Length, 1);
            MarketValue = NaN(Length, 1);
            Commission = NaN(Length, 1);
            PnLPrice = zeros(Length, 1);
            PnLMoney = zeros(Length, 1);
            
            NbData = length(this.Param.Instrument);
            Contracts = cell(NbData, 1);
            LongPosition = zeros(NbData, 1);
            LongMarketValue = zeros(NbData, 1);
            ShortPosition = zeros(NbData, 1);
            ShortMarketValue = zeros(NbData, 1);
            for i = 1 : NbData
                Contracts{i} = GetContract(this.Param.Instrument{i});
            end
            
            for i = 1 : Length
                Trade = Trades(i);
                InstrID = Trade.Order.InstrumentID;
                
                InstrumentID{i} = Contracts{InstrID}.Symbol;
                IsOpen(i) = Trade.Order.IsOpen;
                IsLong(i) = Trade.Order.IsLong;
                Time(i) = Trade.Time;
                Amount(i) = Trade.Amount;
                Price(i) = Trade.Price;
                MarketValue(i) = Amount(i) * Price(i) * Contracts{InstrID}.Multiplier;
                
                if IsOpen(i)
                    Commission(i) = MarketValue(i) * Contracts{InstrID}.OpenCommissionByMoney ...
                        + Trade.Amount * Contracts{InstrID}.OpenCommissionByVolume;
                    if IsLong(i)
                        LongPosition(InstrID) = LongPosition(InstrID) + Trade.Amount;
                        LongMarketValue(InstrID) = LongMarketValue(InstrID) + Amount(i) * Price(i);
                    else
                        ShortPosition(InstrID) = ShortPosition(InstrID) + Trade.Amount;
                        ShortMarketValue(InstrID) = ShortMarketValue(InstrID) + Amount(i) * Price(i);
                    end
                else
                    Commission(i) = MarketValue(i) * Contracts{InstrID}.CloseCommissionByMoney ...
                        + abs(Trade.Amount) * Contracts{InstrID}.CloseCommissionByVolume;
                    if IsLong(i)
                        AvgCost = ShortMarketValue(InstrID) / ShortPosition(InstrID);
                        PnLPrice(i) = Trade.Amount * (AvgCost - Trade.Price);
                        PnLMoney(i) = PnLPrice(i) * Contracts{InstrID}.Multiplier;
                        ShortPosition(InstrID) = ShortPosition(InstrID) - Trade.Amount;
                        ShortMarketValue(InstrID) = ShortMarketValue(InstrID) - AvgCost * Trade.Amount;
                    else
                        AvgCost = LongMarketValue(InstrID) / LongPosition(InstrID);
                        PnLPrice(i) = Trade.Amount * (Trade.Price - AvgCost);
                        PnLMoney(i) = PnLPrice(i) * Contracts{InstrID}.Multiplier;
                        LongPosition(InstrID) = LongPosition(InstrID) - Trade.Amount;
                        LongMarketValue(InstrID) = LongMarketValue(InstrID) - AvgCost * Trade.Amount;
                    end
                end
            end
            
            Time.Format = 'yyyy-MM-dd hh:mm:ss.SSSS';
            this.TradeBook = table(InstrumentID, IsOpen, ...
                IsLong, Time, Amount, Price, MarketValue, ...
                Commission, PnLPrice, PnLMoney);
            
        end
        
        function [ LongAmount, ShortAmount, PnL, Commission ] = DrawChart( this, Time, MktData, PriceAxes )
            % show results and charts of specific instrument
            
            Length = length(Time);
            
            Orders = this.OrderBook(strcmp(this.OrderBook.InstrumentID, MktData.Contract.Symbol), :);
            Trades = this.TradeBook(strcmp(this.TradeBook.InstrumentID, MktData.Contract.Symbol), :);
            
            OrderSubmitTime = Orders.SubmitTime;
            OrderLastTime = Orders.LastTime;
            OrderPrice = Orders.Price;
            OrderCancelled = logical(Orders.IsCancelled);
            OrderColor = zeros(height(Orders), 3);
            OrderColor(logical(Orders.IsLong), 2) = 1;
            OrderColor(~logical(Orders.IsLong), 1) = 1;
            OrderSubmitTimeIdx = NaN(height(Orders), 1);
            OrderLastTimeIdx = NaN(height(Orders), 1);
            for i = 1 : height(Orders)
                OrderSubmitTimeIdx(i) = find(Time >= OrderSubmitTime(i), 1, 'first');
                OrderLastTimeIdx(i) = find(Time >= OrderLastTime(i), 1, 'first');
            end
            
            TradeTime = Trades.Time;
            TradePrice = Trades.Price;
            TradeAmount = Trades.Amount;
            TradeIsLong = logical(Trades.IsLong);
            TradeIsOpen = logical(Trades.IsOpen);
            TradeColor = zeros(height(Trades), 3);
            TradeColor(TradeIsLong, 2) = 1;
            TradeColor(~TradeIsLong, 1) = 1;
            TradeTimeIdx = NaN(height(Trades), 1);
            for i = 1 : height(Trades)
                TradeTimeIdx(i) = find(Time >= TradeTime(i), 1, 'first');
            end
            
            Mid = NaN(Length, 1);
            Bid = NaN(Length, 1);
            Ask = NaN(Length, 1);
            VWAP = NaN(Length, 1);
            LongPeriod = NaN(Length, 1);
            ShortPeriod = NaN(Length, 1);
            LongAmount = zeros(Length, 1);
            ShortAmount = zeros(Length, 1);
            PnL = zeros(Length, 1);
            Commission = zeros(Length, 1);
            NextTickIdx = 1;
            NextTradeIdx = 1;
            LastBid = nan;
            LastAsk = nan;
            LastMid = nan;
            LastVWAP = nan;
            LastPosition = 0;
            for i = 1 : Length
                if NextTickIdx <= MktData.Length && Time(i) >= MktData.Time(NextTickIdx)
                    LastBid = MktData.Bid(NextTickIdx);
                    LastAsk = MktData.Ask(NextTickIdx);
                    LastMid = MktData.Mid(NextTickIdx);
                    LastVWAP = MktData.VWAP(NextTickIdx);
                    NextTickIdx = NextTickIdx + 1;
                end
                if NextTradeIdx <= length(TradeTime) && Time(i) >= TradeTime(NextTradeIdx)
                    if TradeIsLong(NextTradeIdx)
                        LastPosition = LastPosition + TradeAmount(NextTradeIdx);
                        if TradeIsOpen(NextTradeIdx)
                            LongAmount(i) = LongAmount(i) + TradeAmount(NextTradeIdx);
                        else
                            ShortAmount(i) = ShortAmount(i) - TradeAmount(NextTradeIdx);
                        end
                    else
                        LastPosition = LastPosition - TradeAmount(NextTradeIdx);
                        if TradeIsOpen(NextTradeIdx)
                            ShortAmount(i) = ShortAmount(i) + TradeAmount(NextTradeIdx);
                        else
                            LongAmount(i) = LongAmount(i) - TradeAmount(NextTradeIdx);
                        end
                    end
                    PnL(i) = PnL(i) + Trades.PnLMoney(NextTradeIdx);
                    Commission(i) = Commission(i) + Trades.Commission(NextTradeIdx);
                    NextTradeIdx = NextTradeIdx + 1;
                end
                Mid(i) = LastMid;
                Bid(i) = LastBid;
                Ask(i) = LastAsk;
                VWAP(i) = LastVWAP;
                if LastPosition > 0
                    LongPeriod(i) = LastMid;
                elseif LastPosition < 0
                    ShortPeriod(i) = LastMid;
                end
            end
            LongAmount = cumsum(LongAmount);
            ShortAmount = cumsum(ShortAmount);
            PnL = cumsum(PnL);
            Commission = cumsum(Commission);
            
            DateStr = datestr(MktData.DayInfo(1).Day, 'yyyy/mm/dd');
            if length(MktData.DayInfo) > 1
                DateStr = [ DateStr ' - ' datestr(MktData.DayInfo(end).Day, 'yyyy/mm/dd') ];
            end
            
            XAxis = transpose(1 : Length);
            grid on;
            hold on;
            % price chart
            % plot price and VWAP
            title(PriceAxes, [ 'Instrument: ' MktData.Contract.Symbol '  Date: ' DateStr ]);
            xlim(PriceAxes, [ 1 Length ]);
            plot(PriceAxes, XAxis, Bid, 'b');
            plot(PriceAxes, XAxis, Ask, 'b');
            plot(PriceAxes, XAxis, LongPeriod, 'g.-');
            plot(PriceAxes, XAxis, ShortPeriod, 'r.-');
            legend(PriceAxes, { 'Mid Price'; 'Long Period'; 'Short Period' }, 'FontSize', 6, 'Location', 'northwest', 'box', 'off');
            scatter(PriceAxes, XAxis, VWAP, 10, 'm', 'filled');
            % plot orders
            scatter(PriceAxes, OrderSubmitTimeIdx, OrderPrice, 30, OrderColor, 'o');
            scatter(PriceAxes, OrderLastTimeIdx(OrderCancelled), OrderPrice(OrderCancelled), 30, OrderColor(OrderCancelled, :), 'x');
            % plot trades
            scatter(PriceAxes, TradeTimeIdx(TradeIsOpen), TradePrice(TradeIsOpen), 30, TradeColor(TradeIsOpen, :), 'v');
            scatter(PriceAxes, TradeTimeIdx(~TradeIsOpen), TradePrice(~TradeIsOpen), 30, TradeColor(~TradeIsOpen, :), '^');
            hold off;
            
        end
        
        function [ Time, LegPosition, LegPnLPrice, LegPnLMoney, LegCommission ] = DrawSpreadChart( this, DrivingLegData, FollowingLegData, PriceAxes )
            % show results and charts of specific instrument
            
            Time = union(DrivingLegData.Time, FollowingLegData.Time);
            Length = length(Time);
            
            MidSpread = nan(Length, 1);
            [ ~, Idx1 ] = ismember(DrivingLegData.Time, Time, 'legacy');
            [ ~, Idx2 ] = ismember(FollowingLegData.Time, Time, 'legacy');
            MidPrice1 = nan(Length, 1);
            MidPrice1(Idx1) = DrivingLegData.Mid;
            MidPrice2 = nan(Length, 1);
            MidPrice2(Idx2) = FollowingLegData.Mid;
            LastPrice1 = nan;
            LastPrice2 = nan;
            for i = 1 : Length
                if ~isnan(MidPrice1(i))
                    LastPrice1 = MidPrice1(i);
                end
                if ~isnan(MidPrice2(i))
                    LastPrice2 = MidPrice2(i);
                end
                MidSpread(i) = LastPrice1 - LastPrice2;
            end
            
            CurrSpreadPosition = 0;
            CurrLegPosition = zeros(2, 1);
            CurrLegMktValue = zeros(2, 1);
            SpreadPosition = zeros(Length, 1);
            LegPosition = zeros(Length, 4);
            LegPnLPrice = zeros(Length, 4);
            LegPnLMoney = zeros(Length, 4);
            LegCommission = zeros(Length, 4);
            
            TradePoint = [];
            TradePrice = [];
            TradeColor = [];
            TradeEntry = [];
            
            for i = 1 : height(this.TradeBook)
                Trade = this.TradeBook(i, :);
                if ~strcmp(Trade.InstrumentID, DrivingLegData.Contract.Symbol) ...
                && ~strcmp(Trade.InstrumentID, FollowingLegData.Contract.Symbol)
                    continue
                end
                TimeIdx = find(Time >= Trade.Time, 1, 'first');
                if strcmp(Trade.InstrumentID, DrivingLegData.Contract.Symbol)
                    if Trade.IsLong
                        CurrLegPosition(1) = CurrLegPosition(1) + Trade.Amount;
                        CurrLegMktValue(1) = CurrLegMktValue(1) + Trade.Amount * Trade.Price;
                    else
                        CurrLegPosition(1) = CurrLegPosition(1) - Trade.Amount;
                        CurrLegMktValue(1) = CurrLegMktValue(1) - Trade.Amount * Trade.Price;
                    end
                    if Trade.IsOpen
                        if Trade.IsLong
                            LegPosition(TimeIdx, 1) = LegPosition(TimeIdx, 1) + Trade.Amount;
                            LegPnLPrice(TimeIdx, 1) = LegPnLPrice(TimeIdx, 1) + Trade.PnLPrice;
                            LegPnLMoney(TimeIdx, 1) = LegPnLMoney(TimeIdx, 1) + Trade.PnLMoney;
                            LegCommission(TimeIdx, 1) = LegCommission(TimeIdx, 1) + Trade.Commission;
                        else
                            LegPosition(TimeIdx, 2) = LegPosition(TimeIdx, 2) + Trade.Amount;
                            LegPnLPrice(TimeIdx, 2) = LegPnLPrice(TimeIdx, 2) + Trade.PnLPrice;
                            LegPnLMoney(TimeIdx, 2) = LegPnLMoney(TimeIdx, 2) + Trade.PnLMoney;
                            LegCommission(TimeIdx, 2) = LegCommission(TimeIdx, 2) + Trade.Commission;
                        end
                    else
                        if Trade.IsLong
                            LegPosition(TimeIdx, 2) = LegPosition(TimeIdx, 2) - Trade.Amount;
                            LegPnLPrice(TimeIdx, 2) = LegPnLPrice(TimeIdx, 2) + Trade.PnLPrice;
                            LegPnLMoney(TimeIdx, 2) = LegPnLMoney(TimeIdx, 2) + Trade.PnLMoney;
                            LegCommission(TimeIdx, 2) = LegCommission(TimeIdx, 2) + Trade.Commission;
                        else
                            LegPosition(TimeIdx, 1) = LegPosition(TimeIdx, 1) - Trade.Amount;
                            LegPnLPrice(TimeIdx, 1) = LegPnLPrice(TimeIdx, 1) + Trade.PnLPrice;
                            LegPnLMoney(TimeIdx, 1) = LegPnLMoney(TimeIdx, 1) + Trade.PnLMoney;
                            LegCommission(TimeIdx, 1) = LegCommission(TimeIdx, 1) + Trade.Commission;
                        end
                    end
                else
                    if Trade.IsLong
                        CurrLegPosition(2) = CurrLegPosition(2) + Trade.Amount;
                        CurrLegMktValue(2) = CurrLegMktValue(2) + Trade.Amount * Trade.Price;
                    else
                        CurrLegPosition(2) = CurrLegPosition(2) - Trade.Amount;
                        CurrLegMktValue(2) = CurrLegMktValue(2) - Trade.Amount * Trade.Price;
                    end
                    if Trade.IsOpen
                        if Trade.IsLong
                            LegPosition(TimeIdx, 3) = LegPosition(TimeIdx, 3) + Trade.Amount;
                            LegPnLPrice(TimeIdx, 3) = LegPnLPrice(TimeIdx, 3) + Trade.PnLPrice;
                            LegPnLMoney(TimeIdx, 3) = LegPnLMoney(TimeIdx, 3) + Trade.PnLMoney;
                            LegCommission(TimeIdx, 3) = LegCommission(TimeIdx, 3) + Trade.Commission;
                        else
                            LegPosition(TimeIdx, 4) = LegPosition(TimeIdx, 4) + Trade.Amount;
                            LegPnLPrice(TimeIdx, 4) = LegPnLPrice(TimeIdx, 4) + Trade.PnLPrice;
                            LegPnLMoney(TimeIdx, 4) = LegPnLMoney(TimeIdx, 4) + Trade.PnLMoney;
                            LegCommission(TimeIdx, 4) = LegCommission(TimeIdx, 4) + Trade.Commission;
                        end
                    else
                        if Trade.IsLong
                            LegPosition(TimeIdx, 4) = LegPosition(TimeIdx, 4) - Trade.Amount;
                            LegPnLPrice(TimeIdx, 4) = LegPnLPrice(TimeIdx, 4) + Trade.PnLPrice;
                            LegPnLMoney(TimeIdx, 4) = LegPnLMoney(TimeIdx, 4) + Trade.PnLMoney;
                            LegCommission(TimeIdx, 4) = LegCommission(TimeIdx, 4) + Trade.Commission;
                        else
                            LegPosition(TimeIdx, 3) = LegPosition(TimeIdx, 3) - Trade.Amount;
                            LegPnLPrice(TimeIdx, 3) = LegPnLPrice(TimeIdx, 3) + Trade.PnLPrice;
                            LegPnLMoney(TimeIdx, 3) = LegPnLMoney(TimeIdx, 3) + Trade.PnLMoney;
                            LegCommission(TimeIdx, 3) = LegCommission(TimeIdx, 3) + Trade.Commission;
                        end
                    end
                end
                
                if CurrLegPosition(1) * CurrLegPosition(2) < 0
                    AvgPrice1 = CurrLegMktValue(1) / CurrLegPosition(1);
                    AvgPrice2 = CurrLegMktValue(2) / CurrLegPosition(2);
                    PositionChange = min(abs(CurrLegPosition(1)), abs(CurrLegPosition(2)));
                    if CurrLegPosition(1) < 0
                        PositionChange = -PositionChange;
                    end
                    SpreadPosition(TimeIdx) = SpreadPosition(TimeIdx) + PositionChange;
                    
                    TradeSpread = AvgPrice1 - AvgPrice2;
                    TradePoint = [ TradePoint; TimeIdx ];
                    TradePrice = [ TradePrice; TradeSpread ];
                    if PositionChange > 0
                        TradeColor = [ TradeColor; [ 0, 1, 0 ] ];
                    else
                        TradeColor = [ TradeColor; [ 1, 0, 0 ] ];
                    end
                    if CurrSpreadPosition * PositionChange >= 0
                        TradeEntry = [ TradeEntry; true ];
                    else
                        TradeEntry = [ TradeEntry; false ];
                    end
                    CurrSpreadPosition = CurrSpreadPosition + PositionChange;
                    
                    CurrLegPosition(1) = CurrLegPosition(1) - PositionChange;
                    CurrLegMktValue(1) = CurrLegMktValue(1) - AvgPrice1 * PositionChange;
                    CurrLegPosition(2) = CurrLegPosition(2) + PositionChange;
                    CurrLegMktValue(2) = CurrLegMktValue(2) + AvgPrice2 * PositionChange;
                end
            end
            
            for i = 1 : 4
                LegPosition(:, i) = cumsum(LegPosition(:, i));
                LegPnLPrice(:, i) = cumsum(LegPnLPrice(:, i));
                LegPnLMoney(:, i) = cumsum(LegPnLMoney(:, i));
                LegCommission(:, i) = cumsum(LegCommission(:, i));
            end
            SpreadPosition = cumsum(SpreadPosition);
            TradeEntry = logical(TradeEntry);
            
            LongPeriod = NaN(Length, 1);
            ShortPeriod = NaN(Length, 1);
            LongPeriod(SpreadPosition > 0) = MidSpread(SpreadPosition > 0);
            ShortPeriod(SpreadPosition < 0) = MidSpread(SpreadPosition < 0);
            
            DateStr = datestr(DrivingLegData.DayInfo(1).Day, 'yyyy/mm/dd');
            if length(DrivingLegData.DayInfo) > 1
                DateStr = [ DateStr ' - ' datestr(DrivingLegData.DayInfo(end).Day, 'yyyy/mm/dd') ];
            end
            
            XAxis = transpose(1 : Length);
            grid on;
            hold on;
            % plot mid and vwap spread
            title(PriceAxes, [ 'Spread: ' DrivingLegData.Contract.Symbol ' - ' FollowingLegData.Contract.Symbol '  Date: ' DateStr ]);
            xlim(PriceAxes, [ 1 Length ]);
            plot(PriceAxes, XAxis, MidSpread);
            plot(PriceAxes, XAxis, LongPeriod, 'g');
            plot(PriceAxes, XAxis, ShortPeriod, 'r');
            legend(PriceAxes, { 'Mid Spread'; 'Long Period'; 'Short Period' }, ...
                'FontSize', 6, 'Location', 'northwest', 'box', 'off');
            % plot trades
            scatter(PriceAxes, TradePoint(TradeEntry), TradePrice(TradeEntry), 20, TradeColor(TradeEntry, :), 'v');
            scatter(PriceAxes, TradePoint(~TradeEntry), TradePrice(~TradeEntry), 20, TradeColor(~TradeEntry, :), '^');
            hold off;
            
        end
        
    end
    
    methods (Access = public)
        
        function WriteTable( this, Folder )
            
            if nargin < 2; Folder = ''; end
            
            writetable(this.TradeBook, [ Folder 'TradeBook.csv' ]);
            
            OrderBook2 = this.OrderBook;
            OrderBook2.PartialFilledTime = [];
            OrderBook2.PartialFilledAmount = [];
            
            writetable(OrderBook2, [ Folder 'OrderBook.csv' ]);
            writetable(this.ResultStats, [ Folder 'Statistics.csv' ]);
            
        end
        
        function ChartAxes = ShowChart( this, InstrumentID )
            % show results and charts of specific instrument
            
            if nargin < 2; InstrumentID = 1; end
            
            MktData = LoadMktData(this.Param.Instrument{InstrumentID}, this.Param.StartDate, this.Param.EndDate);
            
            ChartName = [ 'Backtest Result: ' MktData.Contract.Symbol ];
            figure('Name', ChartName);
            ChartAxes(3, 1) = axes;
            
            ChartAxes(1) = subtightplot(5, 1, 1 : 3);
            [ LongAmount, ShortAmount, PnL, Commission ] = this.DrawChart(MktData.Time, MktData, ChartAxes(1));
            
            % p&l chart
            ChartAxes(2) = subtightplot(5, 1, 4);
            grid(ChartAxes(2), 'on');
            hold(ChartAxes(2), 'on');
            plot(ChartAxes(2), PnL - Commission);
            plot(ChartAxes(2), PnL);
            legend(ChartAxes(2), { 'Net P&L'; 'Gross P&L (w/o Commission)' }, 'FontSize', 6, 'Location', 'northwest', 'box', 'off');
            hold(ChartAxes(2), 'off');
            
            ChartAxes(3) = subtightplot(5, 1, 5);
            grid(ChartAxes(3), 'on');
            hold(ChartAxes(3), 'on');
            plot(ChartAxes(3), LongAmount, 'g');
            plot(ChartAxes(3), ShortAmount, 'r');
            legend(ChartAxes(3), { 'Long Amount'; 'Short Amount' }, 'FontSize', 6, 'Location', 'northwest', 'box', 'off');
            hold(ChartAxes(3), 'off');
            
            % link axes
            linkaxes(ChartAxes, 'x');
            
            delete(MktData);
            
        end
        
        function ChartAxes = ShowSpreadChart( this, DrivingLegID, FollowingLegID )
            % show results and charts of specific instrument
            
            DrivingLegData = LoadMktData(this.Param.Instrument{DrivingLegID}, this.Param.StartDate, this.Param.EndDate);
            FollowingLegData = LoadMktData(this.Param.Instrument{FollowingLegID}, this.Param.StartDate, this.Param.EndDate);
            
            ChartName = [ 'Backtest Result: ' DrivingLegData.Contract.Symbol ' - ' FollowingLegData.Contract.Symbol ];
            figure('Name', ChartName);
            ChartAxes(5, 1) = axes;
            
            % spread chart
            ChartAxes(1) = subtightplot(8, 1, 1 : 2);
            [ Time, LegPosition, ~, LegPnLMoney, LegCommission ] = this.DrawSpreadChart(DrivingLegData, FollowingLegData, ChartAxes(1));
            
            % driving leg
            ChartAxes(2) = subtightplot(8, 1, 3 : 4);
            this.DrawChart(Time, DrivingLegData, ChartAxes(2));
            
            % following leg
            ChartAxes(3) = subtightplot(8, 1, 5 : 6);
            this.DrawChart(Time, FollowingLegData, ChartAxes(3));
            
            % p&l chart
            ChartAxes(4) = subtightplot(8, 1, 7);
            grid on;
            hold on;
            plot(ChartAxes(4), sum(LegPnLMoney, 2) - sum(LegCommission, 2));
            plot(ChartAxes(4), sum(LegPnLMoney, 2));
            legend(ChartAxes(4), { 'Net P&L'; 'Gross P&L (w/o Commission)' }, 'FontSize', 6, 'Location', 'northwest', 'box', 'off');
            hold off;
            
            % p&l chart
            ChartAxes(5) = subtightplot(8, 1, 8);
            grid on;
            plot(ChartAxes(5), LegPosition);
            legend(ChartAxes(5), { 'Driving Leg Long Position', 'Driving Leg Short Position', ...
                'Following Leg Long Position', 'Following Leg Short Position' }, 'FontSize', 6, 'Location', 'northwest', 'box', 'off');
            
            % link axes
            linkaxes(ChartAxes, 'x');
            
            delete(DrivingLegData);
            delete(FollowingLegData);
            
        end
        
    end
    
end

