classdef Backtest < BacktestResult
%Backtest Summary of this class goes here
%   Detailed explanation goes here
    
    properties
        Tasks
    end
    
    methods
        
        function this = Backtest( Param, Strategy )
            % constructor
            
            if nargin < 2; error('2 arguments required'); end
            
            if ~iscellstr(Param.Instrument)
                error('Invalid instrument'); 
            end
            if isempty(Param.StartDate)
                error('Invalid start date'); 
            end
            if isempty(Param.EndDate) || Param.EndDate < Param.StartDate
                error('Invalid end date'); 
            end
            if Param.ShowTradeInfo && Param.ShowProcessBar
                error('Can not show trade information when active process bar'); 
            end
            if ~isa(Strategy, 'Strategy') || ~isvalid(Strategy)
                error('Invalid strategy object'); 
            end
            
            this.Param = Param;
            this.Strategy = Strategy;
            
            fprintf('Distributing Tasks...\n');
            
            if Param.UseMultiThread
                NbTasks = days(Param.EndDate - Param.StartDate) + 1;
                TasksTmp(NbTasks, 1) = BacktestTask;
                this.Tasks = TasksTmp;
                for i = 1 : length(this.Tasks)
                    TaskParam = Param;
                    TaskDate = Param.StartDate + days(i - 1);
                    TaskParam.StartDate = TaskDate;
                    TaskParam.EndDate = TaskDate;
                    this.Tasks(i).Init(TaskParam, Strategy.Clone());
                end
            else
                this.Tasks = BacktestTask;
                this.Tasks.Init(Param, Strategy.Clone());
            end
            
            fprintf('%d Tasks Built\n', length(this.Tasks));
            fprintf('Backtesting...\n');
            
            if length(this.Tasks) > 1
                gcp;
            end
            
            if Param.ShowProcessBar
                ParforProgress(length(this.Tasks) * 100);
            end
            
            if length(this.Tasks) > 1
                ParTasks = this.Tasks;
                parfor i = 1 : length(ParTasks)
                    SingleTask = ParTasks(i);
                    SingleTask.Run();
                    ParTasks(i) = SingleTask;
                end
                this.Tasks = ParTasks;
            else
                this.Tasks.Run();
            end
            
            if Param.ShowProcessBar
                ParforProgress(0);
            end
            
            fprintf('Aggregating...\n');
            
            % aggregate results
            this.OrderBook = table();
            this.TradeBook = table();
            for i = 1 : length(this.Tasks)
                this.OrderBook = [ this.OrderBook; this.Tasks(i).OrderBook ];
                this.TradeBook = [ this.TradeBook; this.Tasks(i).TradeBook ];
            end
            
            % analyse backtest result
            this.AnalyseResult();
            
            % print key statistics
            if Param.ShowResults
                fprintf('Backtest Result:\n');
                fprintf('                         %15s | %15s | %15s \n', ...
                    'Total', 'Long', 'Short');
                fprintf('  Traded Amount:         %15d | %15d | %15d \n', ...
                    this.ResultStats.AmtTrade, this.ResultStats.AmtTradeLong, this.ResultStats.AmtTradeShort);
                fprintf('  P&L Money:             %15.2f | %15.2f | %15.2f \n', ...
                    this.ResultStats.PnLMoney, this.ResultStats.PnLMoneyLong, this.ResultStats.PnLMoneyShort);
                fprintf('  P&L Money Per Amt:     %15.2f | %15.2f | %15.2f \n', ...
                    this.ResultStats.AvgPnLMoney, this.ResultStats.AvgPnLMoneyLong, this.ResultStats.AvgPnLMoneyShort);
                fprintf('  P&L Price:             %15.2f | %15.2f | %15.2f \n', ...
                    this.ResultStats.PnLPrice, this.ResultStats.PnLPriceLong, this.ResultStats.PnLPriceShort);
                fprintf('  P&L Price Per Amt:     %15.2f | %15.2f | %15.2f \n', ...
                    this.ResultStats.AvgPnLPrice, this.ResultStats.AvgPnLPriceLong, this.ResultStats.AvgPnLPriceShort);
                fprintf('  Win Ratio:             %14.2f%% | %14.2f%% | %14.2f%% \n', ...
                    this.ResultStats.WinRate * 100, this.ResultStats.WinRateLong * 100, this.ResultStats.WinRateShort * 100);
                fprintf('  Max Position:          %15d | %15d | %15d \n', ...
                    this.ResultStats.MaxAmtTrade, this.ResultStats.MaxAmtTradeLong, this.ResultStats.MaxAmtTradeShort);
                fprintf('  Commission:            %15.2f | %15.2f | %15.2f \n', ...
                    this.ResultStats.CommTrade, this.ResultStats.CommTradeLong, this.ResultStats.CommTradeShort);
                fprintf('  Limit Order Placed:    %15d | %15d | %15d \n', ...
                    this.ResultStats.NbLmt, this.ResultStats.NbLmtLong, this.ResultStats.NbLmtShort);
                fprintf('  Limit Order Cancelled: %15d | %15d | %15d \n', ...
                    this.ResultStats.NbLmtCancel, this.ResultStats.NbLmtLongCancel, this.ResultStats.NbLmtShortCancel);
                fprintf('  Limit Amount:          %15d | %15d | %15d \n', ...
                    this.ResultStats.AmtLmt, this.ResultStats.AmtLmtLong, this.ResultStats.AmtLmtShort);
                fprintf('  Limit Amount Filled:   %15d | %15d | %15d \n', ...
                    this.ResultStats.AmtLmtFill, this.ResultStats.AmtLmtLongFill, this.ResultStats.AmtLmtShortFill);
                fprintf('  Limit Amount Cancel:   %15d | %15d | %15d \n', ...
                    this.ResultStats.AmtLmtCancel, this.ResultStats.AmtLmtLongCancel, this.ResultStats.AmtLmtShortCancel);
                fprintf('  Fill Rate:             %14.2f%% | %14.2f%% | %14.2f%% \n', ...
                    this.ResultStats.FillRate * 100, this.ResultStats.FillRateLong * 100, this.ResultStats.FillRateShort * 100);
                fprintf('  Avg Sec To Fill:       %15.2f | %15.2f | %15.2f \n', ...
                    this.ResultStats.AvgSec2Fill, this.ResultStats.AvgSec2FillLong, this.ResultStats.AvgSec2FillShort);
            end
            
        end
        
    end
    
    
end

