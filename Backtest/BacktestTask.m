classdef BacktestTask < BacktestResult
%BacktestTask Summary of this class goes here
%   Detailed explanation goes here

    properties
        ShowTradeInfo
        
        LastTime
        MktData
        Orders
        Trades
        Exchanges
        Positions
        OpenOrders
        CloseOrders
    end
    
    methods (Access = { ?Backtest })
        
        function Init( this, Param, Strategy )
            % constructor
            
            Strategy.RegisterWith(this);
            
            this.Param = Param;
            this.Strategy = Strategy;
            
        end
        
        function Run( this )
            
            % load market data
            NbData = length(this.Param.Instrument);
            MktDataTmp(NbData, 1) = PriceSeries;
            this.MktData = MktDataTmp;
            for i = 1 : NbData
                this.MktData(i) = LoadMktData(this.Param.Instrument{i}, this.Param.StartDate, this.Param.EndDate);
            end
            
            Time = sort(this.MktData(1).Time);
            for i = 2 : NbData
                Time = union(Time, this.MktData(i).Time, 'sorted');
            end
            NbDataTickFlag = false(length(Time), NbData);
            NbDataTickIdx = NaN(length(Time), NbData);
            for i = 1 : NbData
                [ NbDataTickFlag(:, i), NbDataTickIdx(:, i) ] = ismember(Time, this.MktData(i).Time);
            end
            
            this.Orders = [];
            this.Trades = [];
            this.Positions = zeros(NbData, 2);
            this.OpenOrders = zeros(NbData, 2);
            this.CloseOrders = zeros(NbData, 2);
            
            ExchangesTmp(NbData, 1) = SimExchange;
            this.Exchanges = ExchangesTmp;
            for i = 1 : NbData
                this.Exchanges(i).Init(this, this.Param.SizeDecayRate);
            end
            
            % start loop
            ReminderInterval = length(Time) / 100;
            NextReminderPoint = ReminderInterval;
            DayIndex = 0;
            NextDayIndex = 0;
            Snapshots(NbData, 1) = Snapshot;
            for i = 1 : length(Time)
                
                if this.Param.ShowProcessBar && i >= NextReminderPoint
                    ParforProgress;
                    NextReminderPoint = NextReminderPoint + ReminderInterval;
                end
                
                while i >= NextDayIndex
                    DayIndex = DayIndex + 1;
                    NextDayTime = datetime(9999, 1, 1);
                    for j = 1 : NbData
                        IndexTmp = this.MktData(j).DayInfo(DayIndex).Index + this.MktData(j).DayInfo(DayIndex).Length;
                        if IndexTmp <= this.MktData(j).Length
                            NextDayTime = min(NextDayTime, this.MktData(j).Time(IndexTmp));
                        end
                        this.Exchanges(j).Reset( ...
                            this.MktData(j).GetRow(this.MktData(j).DayInfo(DayIndex).Index), ...
                            this.MktData(j).Contract, ...
                            this.MktData(j).DayInfo(DayIndex).LowerLimitPrice, ...
                            this.MktData(j).DayInfo(DayIndex).UpperLimitPrice);
                        this.Strategy.OnInit(j, this.MktData(j).Contract, this.MktData(j).DayInfo(DayIndex));
                    end
                    if NextDayTime == datetime(9999, 1, 1)
                        NextDayIndex = nan;
                    else
                        NextDayIndex = find(Time >= NextDayTime, 1, 'first');
                    end
                end
                
                for j = 1 : NbData
                    if NbDataTickFlag(i, j)
                        Snapshots(j) = this.MktData(j).GetRow(NbDataTickIdx(i, j));
                        this.Exchanges(j).UpdateVWAP(Snapshots(j));
                    end
                end
                
                for j = 1 : NbData
                    if NbDataTickFlag(i, j)
                        this.Exchanges(j).UpdateBook(Snapshots(j));
                    end
                end
                
                this.LastTime = Time(i);
                
                for j = 1 : NbData
                    this.Exchanges(j).Lock();
                end
                
                for j = 1 : NbData
                    if NbDataTickFlag(i, j)
                        this.Strategy.OnMarketDataUpdate(j, Snapshots(j));
                    end
                end
                
                for j = 1 : NbData
                    this.Exchanges(j).Unlock();
                end
                
            end
            
            % close all remining positions
            for i = 1 : NbData
                if this.Positions(i, 1) > 0
                    OrderObj = Order([], i, false, false, true, 0, this.Positions(i, 1));
                    this.Orders = [ this.Orders; OrderObj ];
                    OrderObj.Submit(this.LastTime);
                    this.FillOrder(OrderObj, OrderObj.Amount, Snapshots(i).Bid(1));
                end
                if this.Positions(i, 2) > 0
                    OrderObj = Order([], i, false, true, true, 0, this.Positions(i, 2));
                    this.Orders = [ this.Orders; OrderObj ];
                    OrderObj.Submit(this.LastTime);
                    this.FillOrder(OrderObj, OrderObj.Amount, Snapshots(i).Ask(1));
                end
            end
            
            % aggregate results
            this.CreateOrderBook(this.Orders);
            this.CreateTradeBook(this.Trades);
            
            delete(this.MktData);
            delete(this.Orders);
            delete(this.Trades);
            delete(this.Exchanges);
            
            % analyse backtest result
            this.AnalyseResult();
            
        end
        
    end
        
    methods (Access = { ?Strategy, ?SimExchange, ?SimOrderBook })
        
        function Amount = PlaceOrder( this, Order )
            % place order
            
            if this.ShowTradeInfo
                if Order.IsLong
                    BuySell = 'Buy';
                else
                    BuySell = 'Sell';
                end
                if Order.IsOpen
                    OpenClose = 'Open';
                else
                    OpenClose = 'Close';
                end
                fprintf('New Order: %s %s %d of %s at %f \n', ...
                    BuySell, OpenClose, Order.Amount, ...
                    this.MktData(Order.InstrumentID).Contract.Symbol, Order.Price);
            end
            
            if ~isnan(Order.Price)
                if mod(Order.Price, this.MktData(Order.InstrumentID).Contract.TickSize) ~= 0
                    error('order price incorrect');
                end
            end
            
            if Order.IsOpen
                if Order.IsLong
                    CurrentOpenAmount = sum(this.OpenOrders(:, 1)) + sum(this.Positions(:, 1));
                    if CurrentOpenAmount + Order.Amount > this.Param.MaxPosition
                        warning('exceed max allowed position amount');
                        if CurrentOpenAmount < this.Param.MaxPosition
                            Order.Amount = this.Param.MaxPosition - CurrentOpenAmount;
                        else
                            Amount = 0;
                            return;
                        end
                    end
                    this.OpenOrders(Order.InstrumentID, 1) = this.OpenOrders(Order.InstrumentID, 1) + Order.Amount;
                else
                    CurrentOpenAmount = sum(this.OpenOrders(:, 2)) + sum(this.Positions(:, 2));
                    if CurrentOpenAmount + Order.Amount > this.Param.MaxPosition
                        warning('exceed max allowed position amount');
                        if CurrentOpenAmount < this.Param.MaxPosition
                            Order.Amount = this.Param.MaxPosition - CurrentOpenAmount;
                        else
                            Amount = 0;
                            return;
                        end
                    end
                    this.OpenOrders(Order.InstrumentID, 2) = this.OpenOrders(Order.InstrumentID, 2) + Order.Amount;
                end
            else
                if Order.IsLong
                    this.CloseOrders(Order.InstrumentID, 2) = this.CloseOrders(Order.InstrumentID, 2) + Order.Amount;
                    if this.CloseOrders(Order.InstrumentID, 2) > this.Positions(Order.InstrumentID, 2)
                        error('close amount bigger than position amount');
                    end
                else
                    this.CloseOrders(Order.InstrumentID, 1) = this.CloseOrders(Order.InstrumentID, 1) + Order.Amount;
                    if this.CloseOrders(Order.InstrumentID, 1) > this.Positions(Order.InstrumentID, 1)
                        error('close amount bigger than position amount');
                    end
                end
            end
            
            % record order
            this.Orders = [ this.Orders; Order ];
            this.Exchanges(Order.InstrumentID).InsertOrder(this.LastTime, Order);
            
            Amount = Order.Amount;
            
        end
        
        function CancelOrder( this, Order )
            % cancel order
            
            if this.ShowTradeInfo
                if Order.IsLong
                    BuySell = 'Buy';
                else
                    BuySell = 'Sell';
                end
                if Order.IsOpen
                    OpenClose = 'Open';
                else
                    OpenClose = 'Close';
                end
                fprintf('Cancel Order: %s %s %d of %s at %f \n', ...
                    BuySell, OpenClose, Order.Amount, ...
                    this.MktData(Order.InstrumentID).Contract.Symbol, Order.Price);
            end
            
            if Order.IsOpen
                if Order.IsLong
                    this.OpenOrders(Order.InstrumentID, 1) = this.OpenOrders(Order.InstrumentID, 1) - Order.Amount;
                else
                    this.OpenOrders(Order.InstrumentID, 2) = this.OpenOrders(Order.InstrumentID, 2) - Order.Amount;
                end
            else
                if Order.IsLong
                    this.CloseOrders(Order.InstrumentID, 2) = this.CloseOrders(Order.InstrumentID, 2) - Order.Amount;
                else
                    this.CloseOrders(Order.InstrumentID, 1) = this.CloseOrders(Order.InstrumentID, 1) - Order.Amount;
                end
            end
            
            this.Exchanges(Order.InstrumentID).CancelOrder(this.LastTime, Order);
            
        end
        
        function FillOrder( this, Order, Amount, Price )
            % execute an order
            
            if this.ShowTradeInfo
                if Order.IsLong
                    BuySell = 'Buy';
                else
                    BuySell = 'Sell';
                end
                if Order.IsOpen
                    OpenClose = 'Open';
                else
                    OpenClose = 'Close';
                end
                fprintf('New Trade: %s %s %d of %s at %f \n', ...
                    BuySell, OpenClose, Amount, ...
                    this.MktData(Order.InstrumentID).Contract.Symbol, Price);
            end
            
            if Order.IsOpen
                if Order.IsLong
                    this.Positions(Order.InstrumentID, 1) = this.Positions(Order.InstrumentID, 1) + Amount;
                    this.OpenOrders(Order.InstrumentID, 1) = this.OpenOrders(Order.InstrumentID, 1) - Amount;
                    if sum(this.OpenOrders(:, 1)) + sum(this.Positions(:, 1)) > this.Param.MaxPosition
                        error('exceed max allowed position amount');
                    end
                else
                    this.Positions(Order.InstrumentID, 2) = this.Positions(Order.InstrumentID, 2) + Amount;
                    this.OpenOrders(Order.InstrumentID, 2) = this.OpenOrders(Order.InstrumentID, 2) - Amount;
                    if sum(this.OpenOrders(:, 2)) + sum(this.Positions(:, 2)) > this.Param.MaxPosition
                        error('exceed max allowed position amount');
                    end
                end
            else
                if Order.IsLong
                    this.Positions(Order.InstrumentID, 2) = this.Positions(Order.InstrumentID, 2) - Amount;
                    this.CloseOrders(Order.InstrumentID, 2) = this.CloseOrders(Order.InstrumentID, 2) - Amount;
                else
                    this.Positions(Order.InstrumentID, 1) = this.Positions(Order.InstrumentID, 1) - Amount;
                    this.CloseOrders(Order.InstrumentID, 1) = this.CloseOrders(Order.InstrumentID, 1) - Amount;
                end
            end
            
            Order.Fill(this.LastTime, Amount, Price);
            
            % record trade
            TradeObj = Trade(length(this.Trades) + 1, Order, this.LastTime, Amount, Price);
            this.Trades = [ this.Trades; TradeObj ];
            
        end
        
    end
    
end

