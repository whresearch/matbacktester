classdef BacktestParam
    %BACKTESTPARAM Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Instrument = [];
        StartDate = [];
        EndDate = [];
        MaxPosition = Inf;
        SizeDecayRate = 0.05;
        UseMultiThread = true;
        ShowTradeInfo = false;
        ShowProcessBar = true;
        ShowResults = true;
    end
    
end

