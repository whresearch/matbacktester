classdef Order < handle
    
    properties
        InstrumentID
        IsOpen
        IsLong
        IsIOC
        Price
        Amount
        CustomerExtra
        SubmitTime
        FilledAmount
        PartialFilledTime
        PartialFilledAmount
        Status
        LastTime
    end
    
    properties (Access = private)
        Strategy
    end
    
    methods
        
        function this = Order( Strategy, InstrumentID, IsOpen, IsLong, IsIOC, Price, Amount, Extra )
            
            if nargin < 8; Extra = []; end
            
            if Price == 0
                Price = nan;
            end
            
            if Amount <= 0
                error('order amount incorrect'); 
            end
            if isnan(Price) && ~IsIOC
                error('order price/IOC incorrect'); 
            end
            
            this.Strategy = Strategy;
            
            this.InstrumentID = InstrumentID;
            this.IsOpen = IsOpen;
            this.IsLong = IsLong;
            this.IsIOC = IsIOC;
            this.Price = Price;
            this.Amount = Amount;
            this.CustomerExtra = Extra;
            
            this.SubmitTime = datetime;
            this.SubmitTime.Format = 'yyyy-MM-dd hh:mm:ss.SSS';
            this.FilledAmount = 0;
            this.PartialFilledTime = [];
            this.PartialFilledAmount = [];
            this.Status = OrderStatus.Request;
            this.LastTime = datetime;
            this.LastTime.Format = 'yyyy-MM-dd hh:mm:ss.SSS';
            
        end
        
        function Submit( this, Time )
            
            this.Status = OrderStatus.Ack;
            this.SubmitTime = Time;
            this.LastTime = Time;
            
            if ~isempty(this.Strategy)
                this.Strategy.OnOrderInsertAckImpl(this);
            end
            
        end
        
        function Fill( this, Time, Amount, Price )
            
            this.PartialFilledTime = [ this.PartialFilledTime; Time ];
            this.PartialFilledAmount = [ this.PartialFilledAmount; Amount ];
            
            this.Amount = this.Amount - Amount;
            this.FilledAmount = this.FilledAmount + Amount;
            this.LastTime = Time;
            
            if this.Amount < 0
                this.Status = OrderStatus.Fail;
                error('filled amount bigger than orignal amount'); 
            elseif this.Amount == 0
                this.Status = OrderStatus.Finish;
            end
            
            if ~isempty(this.Strategy)
                this.Strategy.OnOrderFilledImpl(this, Amount, Price);
            end
            
        end
        
        function Cancel( this, Time )
            
            this.Status = OrderStatus.Cancel;
            this.LastTime = Time;
            
            if ~isempty(this.Strategy)
                this.Strategy.OnOrderCancelImpl(this);
            end
            
        end
        
    end
    
end

