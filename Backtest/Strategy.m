classdef (Abstract) Strategy < handle
    %STRATEGY Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        Backtest
        LiveOrders
    end
    
    methods (Abstract)
        
        OnInit( this, InstrumentID, Contract, TradingDay )
        
        OnMarketDataUpdate( this, InstrumentID, MarketData )
        
        OnOrderInsertAck( this, Order )
        
        OnOrderCancel( this, Order )
        
        OnTrade( this, Order, Amount, Price )
        
    end
    
    methods
        
        function Amount = BuyOpen( this, InstrumentID, Amount, Price, IsIOC, Extra )
            
            if nargin < 6; Extra = []; end
            
            OrderObj = Order(this, InstrumentID, true, true, IsIOC, Price, Amount, Extra);
            Amount = this.Backtest.PlaceOrder(OrderObj);
            
        end
        
        function Amount = SellOpen( this, InstrumentID, Amount, Price, IsIOC, Extra )
            
            if nargin < 6; Extra = []; end
            
            OrderObj = Order(this, InstrumentID, true, false, IsIOC, Price, Amount, Extra);
            Amount = this.Backtest.PlaceOrder(OrderObj);
            
        end
        
        function Amount = BuyClose( this, InstrumentID, Amount, Price, IsIOC, Extra )
            
            if nargin < 6; Extra = []; end
            
            OrderObj = Order(this, InstrumentID, false, true, IsIOC, Price, Amount, Extra);
            Amount = this.Backtest.PlaceOrder(OrderObj);
            
        end
        
        function Amount = SellClose( this, InstrumentID, Amount, Price, IsIOC, Extra )
            
            if nargin < 6; Extra = []; end
            
            OrderObj = Order(this, InstrumentID, false, false, IsIOC, Price, Amount, Extra);
            Amount = this.Backtest.PlaceOrder(OrderObj);
            
        end
        
        function CancelOrder( this, Order )
            
            this.Backtest.CancelOrder(Order);
            
        end
        
        function Orders = GetAllOrder( this, InstrumentID, IsLong, IsOpen )
            
            if nargin < 2; InstrumentID = nan; end
            if nargin < 3; IsLong = nan; end
            if nargin < 4; IsOpen = nan; end
            
            Orders = this.LiveOrders;
            
            if ~isnan(InstrumentID)
                for i = length(Orders) : -1 : 1
                    if Orders(i).InstrumentID ~= InstrumentID
                        Orders(i) = [];
                    end
                end
            end
            
            if ~isnan(IsLong)
                for i = length(Orders) : -1 : 1
                    if Orders(i).IsLong ~= IsLong
                        Orders(i) = [];
                    end
                end
            end
            
            if ~isnan(IsOpen)
                for i = length(Orders) : -1 : 1
                    if Orders(i).IsOpen ~= IsOpen
                        Orders(i) = [];
                    end
                end
            end
            
        end
        
        function CancelAllOrder( this, InstrumentID, IsLong, IsOpen )
            
            if nargin < 2; InstrumentID = nan; end
            if nargin < 3; IsLong = nan; end
            if nargin < 4; IsOpen = nan; end
            
            OrderObjs = this.GetAllOrder(InstrumentID, IsLong, IsOpen);
            for i = 1 : length(OrderObjs)
                this.CancelOrder(OrderObjs(i));
            end
            
        end
        
    end
    
    methods (Static)
        
        function [ OpenTime, CloseTime ] = GetTradingSessionOpenCloseTime( Time, Contract )
            [ y, m, d ] = ymd(Time);
            day = datetime(y, m, d);
            if Time.Hour - 24 > Contract.CloseHour(end)
                day = day + 1;
            end
            OpenTime = day + hours(Contract.OpenHour);
            CloseTime = day + hours(Contract.CloseHour);
        end
        
    end
    
    methods (Access = { ?Backtest })
        
        % Make a copy of a handle object.
        function new = Clone( this )
            % Instantiate new object of the same class.
            new = feval(class(this));
 
            % Copy all non-hidden properties.
            p = properties(this);
            for i = 1:length(p)
                new.(p{i}) = this.(p{i});
            end
        end
        
    end
    
    methods (Access = { ?BacktestTask })
        
        function RegisterWith( this, Backtest )
            this.Backtest = Backtest;
        end
        
    end
    
    methods (Access = { ?Order })
        
        function OnOrderInsertAckImpl( this, Order )
            this.LiveOrders = [ this.LiveOrders; Order ];
            this.OnOrderInsertAck(Order);
        end
        
        function OnOrderFilledImpl( this, Order, Amount, Price )
            if Order.Amount == 0
                this.LiveOrders(this.LiveOrders == Order) = [];
            end
            this.OnTrade(Order, Amount, Price);
        end
        
        function OnOrderCancelImpl( this, Order )
            this.LiveOrders(this.LiveOrders == Order) = [];
            this.OnOrderCancel(Order);
        end
        
    end
    
end

