classdef SimOrderBook < handle
%SIMORDERBOOK Summary of this class goes here
%   Detailed explanation goes here
    
    properties
        % Type:
        %   0 - Bid book
        %   1 - Ask book
        Type
        DecayRate
        Backtest
        TickSize
        LowerLimit
        UpperLimit
        
        QueueOldSize
        QueueNewSize
        QueueOrderObj
        QueueOrderPos
        
    end
    
    methods (Access = private)
        
        function Idx = FindIndex( this, Price )
            
            if this.Type == 0
                Idx = round((this.UpperLimit - Price) / this.TickSize + 1); 
            else
                Idx = round((Price - this.LowerLimit) / this.TickSize + 1);
            end
            
        end
        
        function Price = FindPrice( this, Idx )
            
            if this.Type == 0
                Price = this.UpperLimit - (Idx - 1) * this.TickSize; 
            else
                Price = this.LowerLimit + (Idx - 1) * this.TickSize; 
            end
            
        end
        
    end
    
    methods (Access = { ?SimExchange })
        
        function this = SimOrderBook( Backtest, Type, DecayRate )
            % constructor
            
            this.Backtest = Backtest;
            if strcmp(Type, 'Bid')
                this.Type = 0;
            else
                this.Type = 1;
            end
            this.DecayRate = DecayRate;
            
        end
        
        function Reset( this, Contract, LowerLimit, UpperLimit )
            
            this.TickSize = Contract.TickSize;
            this.LowerLimit = LowerLimit;
            this.UpperLimit = UpperLimit;
            
            Width = round((UpperLimit - LowerLimit) / this.TickSize + 1);
            this.QueueOldSize = zeros(Width, 1);
            this.QueueNewSize = zeros(Width, 1);
            this.QueueOrderObj = cell(Width, 1);
            this.QueueOrderPos = cell(Width, 1);
            
        end
        
        function Update( this, ExecutePrice, Prices, Sizes )
            
            if ExecutePrice > 0
                StartIdx = this.FindIndex(ExecutePrice);
                Levels = find(this.QueueNewSize > 0);
                for i = 1 : length(Levels)
                    Idx = Levels(i);
                    if Idx <= StartIdx
                        for j = 1 : length(this.QueueOrderObj{Idx})
                            OrderObj = this.QueueOrderObj{Idx}(j);
                            this.Backtest.FillOrder(OrderObj, OrderObj.Amount, OrderObj.Price);
                        end
                        this.QueueOrderObj{Idx} = [];
                        this.QueueOrderPos{Idx} = [];
                        this.QueueNewSize(Idx) = 0;
                    else
                        MyOrderAmt = 0;
                        for j = 1 : length(this.QueueOrderObj{Idx})
                            this.QueueOrderPos{Idx}(j) = max(MyOrderAmt, ...
                                this.QueueOrderPos{Idx}(j) - round((this.QueueOrderPos{Idx}(j) - MyOrderAmt) * this.DecayRate));
                            MyOrderAmt = MyOrderAmt + this.QueueOrderObj{Idx}(j).Amount;
                        end
                        this.QueueNewSize(Idx) = this.QueueNewSize(Idx) - round((this.QueueOldSize(Idx) - MyOrderAmt) * this.DecayRate);
                    end
                end
            end
            
            if Prices(1) > 0
                StartIdx = this.FindIndex(Prices(1)) - 1;
                this.QueueOldSize(1 : StartIdx) = 0;
                for i = 1 : length(Prices)
                    Idx = StartIdx + i;
                    ReducedAmt = min(0, Sizes(i) - this.QueueOldSize(Idx));
                    MyOrderAmt = 0;
                    for j = 1 : length(this.QueueOrderObj{Idx})
                        this.QueueOrderPos{Idx}(j) = max(MyOrderAmt, this.QueueOrderPos{Idx}(j) + ReducedAmt);
                        MyOrderAmt = MyOrderAmt + this.QueueOrderObj{Idx}(j).Amount;
                    end
                    this.QueueOldSize(Idx) = Sizes(i);
                    this.QueueNewSize(Idx) = Sizes(i) + MyOrderAmt;
                    if this.QueueNewSize(Idx) < 0
                        error('should not enter here');
                    end
                end
            end
            
        end
        
        function MatchOrder( this, Order, IsVWAPOrder )
            % match a order based on current limit order book state
            
            RemainingAmount = Order.Amount;
            TradeAmount = 0;
            Value = 0;
            
            % filter levels
            Levels = find(this.QueueNewSize > 0);
            if ~isnan(Order.Price)
                Levels = Levels(Levels <= this.FindIndex(Order.Price));
            end

            % go through filtered levels
            for i = 1 : length(Levels)
                Idx = Levels(i);
                Amount = min(this.QueueNewSize(Idx), RemainingAmount);
                RemainingAmount = RemainingAmount - Amount;
                TradeAmount = TradeAmount + Amount;
                Value = Value + Amount * this.FindPrice(Idx);
                this.QueueNewSize(Idx) = this.QueueNewSize(Idx) - Amount;
                
                j = 1;
                while j <= length(this.QueueOrderObj{Idx})
                    if this.QueueOrderPos{Idx}(j) < Amount
                        OrderObj = this.QueueOrderObj{Idx}(j);
                        Amount2 = min(OrderObj.Amount, Amount - this.QueueOrderPos{Idx}(j));
                        if Amount2 == OrderObj.Amount
                            this.QueueOrderObj{Idx}(j) = [];
                            this.QueueOrderPos{Idx}(j) = [];
                            j = j - 1;
                        else
                            this.QueueOrderPos{Idx}(j) = 0;
                        end
                        this.Backtest.FillOrder(OrderObj, Amount2, OrderObj.Price);
                    else
                        this.QueueOrderPos{Idx}(j) = this.QueueOrderPos{Idx}(j) - Amount;
                    end
                    j = j + 1;
                end
                
                if RemainingAmount == 0
                    break;
                end
            end
            
            
            if IsVWAPOrder
                RemainingAmount = Order.Amount;
                Levels = find(this.QueueOldSize > 0);
                if ~isnan(Order.Price)
                    Levels = Levels(Levels <= this.FindIndex(Order.Price));
                end
                for i = 1 : length(Levels)
                    Idx = Levels(i);
                    Amount = min(this.QueueOldSize(Idx), RemainingAmount);
                    RemainingAmount = RemainingAmount - Amount;
                    this.QueueOldSize(Idx) = this.QueueOldSize(Idx) - Amount;
                    if RemainingAmount == 0
                        break;
                    end
                end
            elseif TradeAmount > 0
                % execute order
                AvgPrice = Value / TradeAmount;
                this.Backtest.FillOrder(Order, TradeAmount, AvgPrice);
            end
            
        end
        
        function AppendOrder( this, Order )
            
            if ~Order.IsIOC && ~isnan(Order.Price)
                if Order.Price < this.LowerLimit || Order.Price > this.UpperLimit
                    error('order price incorrect'); 
                end

                Idx = this.FindIndex(Order.Price);
                if isempty(this.QueueOrderObj{Idx})
                    this.QueueOrderObj{Idx} = Order;
                    this.QueueOrderPos{Idx} = this.QueueNewSize(Idx);
                else
                    this.QueueOrderObj{Idx} = [ this.QueueOrderObj{Idx}; Order ];
                    this.QueueOrderPos{Idx} = [ this.QueueOrderPos{Idx}; this.QueueNewSize(Idx) ];
                end
                this.QueueNewSize(Idx) = this.QueueNewSize(Idx) + Order.Amount;
            end
            
        end
        
        function CancelOrder( this, Order )
            
            if ~Order.IsIOC && ~isnan(Order.Price)
                Idx = this.FindIndex(Order.Price);
                this.QueueNewSize(Idx) = this.QueueNewSize(Idx) - Order.Amount;
                Pos = find(this.QueueOrderObj{Idx} == Order, 1, 'first');
                this.QueueOrderPos{Idx}(Pos) = [];
                this.QueueOrderObj{Idx}(Pos) = [];
                for i = Pos : length(this.QueueOrderPos{Idx})
                    this.QueueOrderPos{Idx}(i) = this.QueueOrderPos{Idx}(i) - Order.Amount;
                end
            end
            
        end
            
    end
    
end

