classdef Trade < handle
    
    properties
        TradeID
        Order
        Time
        Amount
        Price
    end
    
    methods
        
        function this = Trade( TradeID, Order, Time, Amount, Price )
            
            this.TradeID = TradeID;
            this.Order = Order;
            this.Time = Time;
            this.Time.Format = 'yyyy-MM-dd hh:mm:ss.SSS';
            this.Amount = Amount;
            this.Price = Price;
            
        end
        
    end
    
end

