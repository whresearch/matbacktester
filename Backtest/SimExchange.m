classdef SimExchange < handle
    %LIMITORDERBOOK Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        % VWAP Modes: 
        %   0 - Four Levels: Closet four price levels around VWAP
        %   1 - Two Levels: Bid/Ask of current tick
        %   2 - (Default) Two Levels: Bid/Ask of previous tick
        %   3 - One Level: VWAP only
        VWAPMode
        Backtest
        TickSize
        MktData
        BidOrderBook
        AskOrderBook
        OrderAcks
        NbCancel
        Locked
    end
    
    methods (Access = { ?BacktestTask })
        
        function Init( this, Backtest, DecayRate )
            % constructor
            
            this.Backtest = Backtest;
            this.BidOrderBook = SimOrderBook(Backtest, 'Bid', DecayRate);
            this.AskOrderBook = SimOrderBook(Backtest, 'Ask', DecayRate);
            
        end
        
        function Reset( this, MarketData, Contract, LowerPrice, UpperPrice )
            
            this.BidOrderBook.Reset(Contract, LowerPrice, UpperPrice);
            this.AskOrderBook.Reset(Contract, LowerPrice, UpperPrice);
            
            this.TickSize = Contract.TickSize;
            this.MktData = MarketData;
            
            this.OrderAcks = cell(0, 0);
            this.NbCancel = 0;
            this.Locked = false;
            
        end
        
        function Lock( this )
            
            if ~this.Locked
                this.Locked = true;
            end
            
        end
        
        function Unlock( this )
            
            if this.Locked
                this.Locked = false;
                for i = 1 : size(this.OrderAcks, 1)
                    IsInsert = this.OrderAcks{i, 1};
                    Time = this.OrderAcks{i, 2};
                    Order = this.OrderAcks{i, 3};
                    if IsInsert
                        this.InsertOrder(Time, Order);
                    else
                        this.CancelOrder(Time, Order);
                    end
                end
                this.OrderAcks = cell(0, 0);
            end
            
        end
        
        function UpdateVWAP( this, MarketData )
            
            if isnan(MarketData.Last)
                return;
            end
                
            % match VWAP orders
            if MarketData.Volume > 0
                [ Price, Amount, IsLong ] = SplitVWAP(MarketData, this.MktData, 2);
                for i = 1 : length(Price)
                    OrderObj = Order([], [], true, IsLong(i), true, Price(i), Amount(i));
                    if IsLong(i)
                        this.AskOrderBook.MatchOrder(OrderObj, true);
                    else
                        this.BidOrderBook.MatchOrder(OrderObj, true);
                    end
                end
            end
            
        end
        
        function UpdateBook( this, MarketData )
            
            if isnan(MarketData.Last)
                return;
            end
                
            this.BidOrderBook.Update(MarketData.Ask(1), MarketData.Bid, MarketData.BidSize);
            this.AskOrderBook.Update(MarketData.Bid(1), MarketData.Ask, MarketData.AskSize);
            
            % save snapshot
            this.MktData = MarketData;
            
        end
        
        function InsertOrder( this, Time, Order )
            
            if this.Locked
                this.OrderAcks = [ this.OrderAcks; { true, Time, Order } ];
            else
                Order.Submit(Time);
                % match order
                if Order.IsLong
                    this.AskOrderBook.MatchOrder(Order, false);
                else
                    this.BidOrderBook.MatchOrder(Order, false);
                end
                if Order.Amount > 0
                    if Order.IsIOC
                        % cancel unfilled IOC order 
                        this.Backtest.CancelOrder(Order);
                    elseif Order.Price > 0
                        % add active limit order into order book
                        if Order.IsLong
                            this.BidOrderBook.AppendOrder(Order);
                        else
                            this.AskOrderBook.AppendOrder(Order);
                        end
                    end
                end
            end
            
        end
        
        function CancelOrder( this, Time, Order )
            
            if this.Locked
                this.OrderAcks = [ this.OrderAcks; { false, Time, Order } ];
            else
                this.NbCancel = this.NbCancel + 1;
                if this.NbCancel > 500
                    warning('Order cancellation %d exceeds limit', this.NbCancel);
                end
                Order.Cancel(Time);
                if Order.IsLong
                    this.BidOrderBook.CancelOrder(Order);
                else
                    this.AskOrderBook.CancelOrder(Order);
                end
            end
            
        end
            
    end
    
end

