classdef mode < int32
% C++ Order Status
%     const int ORDER_STATUS_REQUEST = 0;
%     const int ORDER_STATUS_ACK = 1;
%     const int ORDER_STATUS_FINISH = 2;
%     const int ORDER_STATUS_CANCEL = 3;
%     const int ORDER_STATUS_CANCEL_REQUEST = 4;
%     const int ORDER_STATUS_FAIL = 5;

    enumeration
      debug (0)
      Ack (1) 
      Finish (2) 
      Cancel (3) 
      Fail (-1)
    end
    
end

