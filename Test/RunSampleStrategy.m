% backtest sample strategy

tic;

% prepare parameters
Param = BacktestParam;
Param.Instrument = { 'cu1510' };
Param.StartDate = datetime(2015, 8, 20);
Param.EndDate = datetime(2015, 8, 20);

% create strategy
Strategy = SampleStrategy();

% calculate results
Result = Backtest(Param, Strategy);

Result.ShowChart();

toc;