classdef SampleStrategy < Strategy
%SampleStrategy Summary of this class goes here
%   Detailed explanation goes here
    
    properties
        Values
        ShortPeriod
        LongPeriod
        LiveAmount
        OrderHoldingPeriod
        EveSessionClose
        DaySessionClose
    end
    
    methods
        
        function this = SampleStrategy()
            this.ShortPeriod = 60;
            this.LongPeriod = 300;
        end
        
        function OnInit( this, InstrumentID, Contract, DayInfo )
            
            % get trading open & close time
            [ ~, CloseTime ] = this.GetTradingSessionOpenCloseTime(DayInfo.Day, Contract);
            this.EveSessionClose = CloseTime(1);
            this.DaySessionClose = CloseTime(end);
            
            this.LiveAmount = 0;
            this.OrderHoldingPeriod = nan;
            this.Values = NaN(this.LongPeriod, 1);
            
        end
        
        function OnMarketDataUpdate( this, InstrumentID, MarketData )
            
            % store mid price and increase holding period
            this.Values = [ MarketData.Mid; this.Values(1 : end - 1) ];
            this.OrderHoldingPeriod = this.OrderHoldingPeriod + 1;
            
            Min2EveClose = minutes(this.EveSessionClose - MarketData.Time);
            Min2DayClose = minutes(this.DaySessionClose - MarketData.Time);
            
            if ~isnan(this.Values(end))
                
                SMAS = mean(this.Values(1 : this.ShortPeriod));
                SMAL = mean(this.Values(1 : this.LongPeriod));
                
                if SMAS > SMAL && isnan(this.OrderHoldingPeriod) && this.LiveAmount >= 0
                    if this.LiveAmount > 0
                        this.SellClose(InstrumentID, this.LiveAmount, 0, true);
                    elseif (Min2EveClose > 5 || Min2EveClose < 0) && Min2DayClose > 5
                        this.SellOpen(InstrumentID, 1, MarketData.Ask(1), false);
                    end
                elseif SMAS < SMAL && isnan(this.OrderHoldingPeriod) && this.LiveAmount <= 0
                    if this.LiveAmount < 0
                        this.BuyClose(InstrumentID, -this.LiveAmount, 0, true);
                    elseif (Min2EveClose > 5 || Min2EveClose < 0) && Min2DayClose > 5
                        this.BuyOpen(InstrumentID, 1, MarketData.Bid(1), false);
                    end
                end
            end
            
            if this.OrderHoldingPeriod > 20
                this.CancelAllOrder();
            end
            
        end
        
        function OnOrderInsertAck( this, Order )
            
            if Order.IsOpen
                this.OrderHoldingPeriod = 0;
            end
            
        end
        
        function OnOrderCancel( this, Order )
            
            if Order.IsOpen
                this.OrderHoldingPeriod = nan;
            end
            
        end
        
        function OnTrade( this, Order, Amount, Price )
            
            if Order.IsLong
                this.LiveAmount = this.LiveAmount + Amount;
            else
                this.LiveAmount = this.LiveAmount - Amount;
            end
            
            if Order.IsOpen
                this.OrderHoldingPeriod = nan;
            end
            
        end
        
    end
    
end

