classdef PriceSeriesDayInfo
    
    properties
        Day
        Index
        Length
        Open
        UpperLimitPrice
        LowerLimitPrice
    end
    
    methods
        function this = PriceSeriesDayInfo( Day, Index, Length, Open, UpperLimitPrice, LowerLimitPrice )
            this.Day = Day;
            this.Index = Index;
            this.Length = Length;
            this.Open = Open;
            this.UpperLimitPrice = UpperLimitPrice;
            this.LowerLimitPrice = LowerLimitPrice;
        end
    end
    
end

