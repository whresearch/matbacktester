classdef Contract
    %Contract Contains all information of the specific contract
    
    properties
        Symbol
        Exchange
        Multiplier
        TickSize
        MaxDepth
        OpenHour
        CloseHour
        OpenCommissionByMoney
        OpenCommissionByVolume
        CloseCommissionByMoney
        CloseCommissionByVolume
    end
    
    methods
        function this = Contract( Symbol, Exchange, Multiplier, TickSize, MaxDepth, ...
                OpenHour, CloseHour, OpenCommissionByMoney, OpenCommissionByVolume, ...
                CloseCommissionByMoney, CloseCommissionByVolume)
            
            if length(OpenHour) ~= length(CloseHour)
                error('open and close hour format inconsistent')
            end
            
            this.Symbol = Symbol;
            this.Exchange = Exchange;
            this.Multiplier = Multiplier;
            this.TickSize = TickSize;
            this.MaxDepth = MaxDepth;
            this.OpenHour = OpenHour;
            this.CloseHour = CloseHour;
            this.OpenCommissionByMoney = OpenCommissionByMoney;
            this.OpenCommissionByVolume = OpenCommissionByVolume;
            this.CloseCommissionByMoney = CloseCommissionByMoney;
            this.CloseCommissionByVolume = CloseCommissionByVolume;
            
        end
    end
    
end

