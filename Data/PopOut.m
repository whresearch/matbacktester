function [ time, tickSize, bidSize, bidPrice, askPrice, mid, askSize, volume, VWAP, last  ] = PopOut( Data )
time = Data.Time;
tickSize = Data.Contract.TickSize;
bidSize = Data.BidSize;
bidPrice = Data.Bid;
askPrice = Data.Ask;
mid = Data.Mid;
askSize = Data.AskSize;
volume = Data.Volume;
VWAP = Data.VWAP;
last = Data.Last;

end

