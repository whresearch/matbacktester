function [ Output ] = LoadMat_SHFE( File )
% LoadMat_SHFE loads a single matlab data file using SHFE settings

% load mat file
data = load(File);

% get name and date
strstart = strfind(File, '_');
strend = strfind(File, '.');
name = File(strstart(end) + 1 : strend - 1);
datestr = File(strstart(1) + 1 : strstart(2) - 1);

% create price series
Contract = GetContract(name);
Output = PriceSeries;
Output.Init(Contract);

% get open, upper limir and lower limit prices
open = data.OpenPrice(end);
upperlimit = data.UpperLimitPrice(end);
lowerlimit = data.LowerLimitPrice(end);

% calcualte datetime
d = datetime(datestr, 'InputFormat', 'yyyyMMdd');
h = floor(data.SecondOfDay / 3600);
m = floor(data.SecondOfDay / 60) - h .* 60;
s = double(data.SecondOfDay - h .* 3600 - m .* 60) + double(data.UpdateMillisec) ./ 1000;
dt = d + duration(h, m, s);

% calcualte open interest
oi = double(data.OpenInterest);
if oi(1) > 50000
    oi(2 : end) = oi(2 : end) - oi(1 : end - 1);
end
oi(1) = 0;

% calculate volume
volume = double(data.Volume);
if volume(end) > 10000 
    volume(2 : end) = volume(2 : end) - volume(1 : end - 1);
end

% calculate turnover
turnover = double(data.Turnover);
turnover(2 : end) = turnover(2 : end) - turnover(1 : end - 1);

% append data
Output.Append(...
    d, ...                          & day
    open, ...                       % open price
    upperlimit, ...                 % upper limit price
    lowerlimit, ...                 % lower limit price
    dt, ...                         % time
    double(data.LastPrice), ...     % last
    double(data.BidPrice1), ...     % bid
    double(data.AskPrice1), ...     % ask
    double(data.BidVolume1), ...    % bid size
    double(data.AskVolume1), ...    % ask size
    oi, ...                         % open interest
    volume, ...                     % volume
    turnover);                      % turnover

end