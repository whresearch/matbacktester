function [ Output ] = LoadMat_SHFE_Range( Symbol, StartDate, EndDate )
% LoadMat_SHFE_Range loads all matlab data file using SHFE settings of a
% specific contract between a time period

% prepare prefix
Prefix = 'shfe_';

% create price series
Contract = GetContract(Symbol);
Output = PriceSeries;
Output.Init(Contract);

% start loop
Date = StartDate;
while Date <= EndDate
    File = strcat(Prefix, datestr(Date, 'yyyymmdd'), '_', Symbol, '.mat');
    % check if file exists
    if exist(File, 'file')
        Output.AppendPriceSeries(LoadMat_SHFE(File));
    end
    % move to next date
    Date = Date + 1;
end

end

