classdef PriceSeries < handle
    %PriceSeries contains all price info
    %   Contract:       Contract Details
    %   DayInfo:
    %   Length:         Size of Contained Snapshots
    %   Time:           Date and Time
    %   Last:           Last Prices
    %   Bid:            Bid Prices
    %   Ask:            Ask Prices
    %   Mid:            Mid Prices
    %   SWMid:          Size Weighted Mid Prices
    %   BidSize:        Bid Sizes
    %   AskSize:        Ask Sizes
    %   OpenInterest:   Open Interest Change
    %   Volume:         Volume
    %   OpenVolume:     Open Volume
    %   CloseVolume:    Close Volume
    %   Turnover:       Turnover
    %   VWAP:           Volume Weighted Average Prices
    
    properties
        Contract
        DayInfo
        Length
        Time
        Last
        Bid
        Ask
        Mid
        SWMid
        BidSize
        AskSize
        OpenInterest
        Volume
        OpenVolume
        CloseVolume
        Turnover
        VWAP
    end
    
    methods
        
        function Init( this, Contract )
            % Construct a price series object
            
            if nargin < 2; error('2 arguments required'); end
            
            this.Length = 0;
            this.Contract = Contract;
        end
        
        function Append( this, Day, Open, UpperLimitPrice, LowerLimitPrice, ...
                Time, Last, Bid, Ask, BidSize, AskSize, OpenInterest, Volume, Turnover )
            % Append new data to price series
            
            if ~isa(this, 'PriceSeries') || isempty(this)
                error(message('invalid input PriceSeries'));
            end
            if nargin ~= 14; error('14 arguments required'); end
            if size(Bid, 2) ~= this.Contract.MaxDepth; error('argument Bid incorrect size'); end
            if size(Ask, 2) ~= this.Contract.MaxDepth; error('argument Ask incorrect size'); end
            if size(BidSize, 2) ~= this.Contract.MaxDepth; error('argument BidSize incorrect size'); end
            if size(AskSize, 2) ~= this.Contract.MaxDepth; error('argument AskSize incorrect size'); end
            
            this.DayInfo = [ this.DayInfo; PriceSeriesDayInfo(...
                Day, this.Length + 1, length(Time), Open, UpperLimitPrice, LowerLimitPrice) ];
            this.Time = [ this.Time; Time ];
            this.Last = [ this.Last; Last ];
            this.Bid = [ this.Bid; Bid ];
            this.Ask = [ this.Ask; Ask ];
            this.Mid = [ this.Mid; (Bid(:, 1) + Ask(:, 1)) / 2 ];
            this.SWMid = [ this.SWMid; ...
                (Ask(:, 1) .* BidSize(:, 1) + Bid(:, 1) .* AskSize(:, 1)) ...
                ./ (BidSize(:, 1) + AskSize(:, 1)) ];
            this.BidSize = [ this.BidSize; BidSize ];
            this.AskSize = [ this.AskSize; AskSize ];
            this.OpenInterest = [ this.OpenInterest; OpenInterest ];
            this.Volume = [ this.Volume; Volume ];
            this.OpenVolume = [ this.OpenVolume; (Volume + OpenInterest) / 2 ];
            this.CloseVolume = [ this.CloseVolume; (Volume - OpenInterest) / 2 ];
            this.Turnover = [ this.Turnover; Turnover ];
            this.VWAP = [ this.VWAP; Turnover ./ Volume / this.Contract.Multiplier ];
            this.Length = this.Length + length(Time);
            
            this.Time.Format = 'yyyy-MM-dd HH:mm:ss.SSS';
        end
        
        function AppendPriceSeries( this, Data )
            % Append new price series to current price series
            
            if ~isa(this, 'PriceSeries') || isempty(this)
                error(message('invalid input PriceSeries'));
            end
            if ~isa(Data, 'PriceSeries') || isempty(Data)
                error(message('invalid input Data'));
            end
            if this.Contract.Symbol ~= Data.Contract.Symbol
                error(message('invalid input Data'));
            end
            
            Infos = Data.DayInfo;
            for i = 1 : length(Infos)
                Infos(i).Index = Infos(i).Index + this.Length;
            end
            this.DayInfo = [ this.DayInfo; Infos ];
            this.Time = [ this.Time; Data.Time ];
            this.Last = [ this.Last; Data.Last ];
            this.Bid = [ this.Bid; Data.Bid ];
            this.Ask = [ this.Ask; Data.Ask ];
            this.Mid = [ this.Mid; Data.Mid ];
            this.SWMid = [ this.SWMid; Data.SWMid ];
            this.BidSize = [ this.BidSize; Data.BidSize ];
            this.AskSize = [ this.AskSize; Data.AskSize ];
            this.OpenInterest = [ this.OpenInterest; Data.OpenInterest ];
            this.Volume = [ this.Volume; Data.Volume ];
            this.OpenVolume = [ this.OpenVolume; Data.OpenVolume ];
            this.CloseVolume = [ this.CloseVolume; Data.CloseVolume ];
            this.Turnover = [ this.Turnover; Data.Turnover ];
            this.VWAP = [ this.VWAP; Data.VWAP ];
            this.Length = this.Length + length(Data.Time);
            
            this.Time.Format = 'yyyy-MM-dd HH:mm:ss.SSS';
        end
        
        function Row = GetRow( this, RowIdx )
            % Construct a price series object
            
            if RowIdx > this.Length; error('invalid RowIdx'); end
            
            Row = Snapshot;
            Row.Time = this.Time(RowIdx);
            Row.Last = this.Last(RowIdx);
            Row.Bid = this.Bid(RowIdx);
            Row.Ask = this.Ask(RowIdx);
            Row.Mid = this.Mid(RowIdx);
            Row.SWMid = this.SWMid(RowIdx);
            Row.BidSize = this.BidSize(RowIdx);
            Row.AskSize = this.AskSize(RowIdx);
            Row.OpenInterest = this.OpenInterest(RowIdx);
            Row.Volume = this.Volume(RowIdx);
            Row.OpenVolume = this.OpenVolume(RowIdx);
            Row.CloseVolume = this.CloseVolume(RowIdx);
            Row.Turnover = this.Turnover(RowIdx);
            Row.VWAP = this.VWAP(RowIdx);
        end
        
        function Output = SplitByDay( this )
            % Split data
            
            Output(length(this.DayInfo), 1) = PriceSeries;
            for i = 1 : length(Output)
                Info = this.DayInfo(i);
                Idx = Info.Index - 1 + (1 : Info.Length);
                Output(i).Init(this.Contract);
                Output(i).Append(...
                    Info.Day, ...
                    Info.Open, ...
                    Info.UpperLimitPrice, ...
                    Info.LowerLimitPrice, ...
                    this.Time(Idx), ...
                    this.Last(Idx), ...
                    this.Bid(Idx), ...
                    this.Ask(Idx), ...
                    this.BidSize(Idx), ...
                    this.AskSize(Idx), ...
                    this.OpenInterest(Idx), ...
                    this.Volume(Idx), ...
                    this.Turnover(Idx));
            end
            
        end
        
        function FilterByPrice( this, PriceType )
            % Filter out duplicated snapshots with specific price type
            
            if ~isa(this, 'PriceSeries') || isempty(this)
                error(message('InvalidInputPriceSeries'));
            end
            if nargin ~= 2; error('2 arguments required'); end
            
            idx = true(length(this.Time), 1);
            
            switch PriceType
                case 'Last'
                    for i = 2 : length(this.Time)
                        if this.Last(i - 1) == this.Last(i)
                            idx(i) = false;
                        end
                    end
                case 'Bid'
                    for i = 2 : length(this.Time)
                        if this.Bid(i - 1) == this.Bid(i)
                            idx(i) = false;
                        end
                    end
                case 'Ask'
                    for i = 2 : length(this.Time)
                        if this.Ask(i - 1) == this.Ask(i)
                            idx(i) = false;
                        end
                    end
                case 'Mid'
                    for i = 2 : length(this.Time)
                        if this.Ask(i - 1) + this.Bid(i - 1) == this.Ask(i) + this.Bid(i)
                            idx(i) = false;
                        end
                    end
                otherwise
                    error(message('invalid input Type'));
            end
            
            j = 1;
            for i = 1 : length(this.DayInfo)
                Info = this.DayInfo(i);
                Info.Length = find(idx >= Info.Index + Info.Length, 1) - j;
                Info.Index = j;
                j = j + Info.Length;
            end
            
            this.Time = this.Time(idx);
            this.Last = this.Last(idx);
            this.Bid = this.Bid(idx);
            this.Ask = this.Ask(idx);
            this.Mid = this.Mid(idx);
            this.SWMid = this.SWMid(idx);
            this.BidSize = this.BidSize(idx);
            this.AskSize = this.AskSize(idx);
            this.OpenInterest = this.OpenInterest(idx);
            this.Volume = this.Volume(idx);
            this.OpenVolume = this.OpenVolume(idx);
            this.CloseVolume = this.CloseVolume(idx);
            this.Turnover = this.Turnover(idx);
            this.VWAP = this.VWAP(idx);
            this.Length = length(this.Time);
        end
        
        function FilterByHours( this, StartHours, EndHours )
            % Filter out snapshots out of trading hours
            
            if ~isa(this, 'PriceSeries') || isempty(this)
                error(message('InvalidInputPriceSeries'));
            end
            if nargin ~= 3; error('3 arguments required'); end
            
            [ h, m, s ] = hms(this.Time);
            t = h + m / 60 + s / 3600;
            idx = false(length(t), 1);
            for i = 1 : length(StartHours)
                if StartHours(i) < 0 || StartHours(i) > 24; error('argument StartHour incorrect'); end
                if EndHours(i) < 0 || EndHours(i) > 24; error('argument EndHour incorrect'); end
                if StartHours(i) >= EndHours(i); error('argument EndHour incorrect'); end
                idx = idx | (t >= StartHours(i) & t <= EndHours(i));
            end
            
            j = 1;
            
            for i = 1 : length(this.DayInfo)
                Info = this.DayInfo(i);
                Info.Length = find(idx >= Info.Index + Info.Length, 1) - j;
                Info.Index = j;
                j = j + Info.Length;
            end
            
            this.Time = this.Time(idx);
            this.Last = this.Last(idx);
            this.Bid = this.Bid(idx);
            this.Ask = this.Ask(idx);
            this.Mid = this.Mid(idx);
            this.SWMid = this.SWMid(idx);
            this.BidSize = this.BidSize(idx);
            this.AskSize = this.AskSize(idx);
            this.OpenInterest = this.OpenInterest(idx);
            this.Volume = this.Volume(idx);
            this.OpenVolume = this.OpenVolume(idx);
            this.CloseVolume = this.CloseVolume(idx);
            this.Turnover = this.Turnover(idx);
            this.VWAP = this.VWAP(idx);
            this.Length = length(this.Time);
        end
        
    end
    
end

