% This script converts all .mat files to uniform data with two snapshots
% per second and following SHFE trading hours

% clear workspace
clear;

% list all files
InputParent = 'D:\Data\MatData_Uniform\';

Files = [];
Exchanges = dir(InputParent);
for i = 3 : length(Exchanges)
    Months = dir([ InputParent Exchanges(i).name ]);
    for j = 3 : length(Months)
        Instruments = dir([ InputParent Exchanges(i).name '\' Months(j).name ]);
        for z = 3 : length(Instruments)
            Content = dir([ InputParent Exchanges(i).name ...
                '\' Months(j).name '\' Instruments(z).name '\shfe_*.mat' ]);
            for k = 1 : length(Content)
                Files = [ Files; [ Exchanges(i).name '\' Months(j).name ...
                    '\' Instruments(z).name '\' Content(k).name ] ];
            end
        end
    end
end

% start loop
for i = 1 : size(Files, 1)
    
    InputFile = Files(i, :);
    Symbol = lower(InputFile(find(InputFile == '_', 1, 'last') + 1 : find(InputFile == '.', 1, 'first') - 5));
    
    Data = load([ InputParent InputFile ]);
    
    Contract = GetContractFromSymbol(Symbol);
    DataLength = 0;
    for j = 1 : length(Contract.OpenHour)
        DataLength = DataLength + 1 + (Contract.CloseHour(j) - Contract.OpenHour(j)) * 7200;
    end
    
    if length(Data.AskPrice1) ~= DataLength
        fprintf('%s \n', InputFile);
    end
    
end