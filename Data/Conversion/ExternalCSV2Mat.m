% This script converts all .mat files to uniform data with two snapshots
% per second and following SHFE trading hours

% clear workspace
clear;

% list all files
InputParent = 'D:\Data\CSVData\';
OutputParent = 'D:\Data\MatData_Raw\';

Files = [];
Exchanges = dir(InputParent);
for i = 3 : length(Exchanges)
    Months = dir([ InputParent Exchanges(i).name ]);
    for j = 3 : length(Months)
        Instruments = dir([ InputParent Exchanges(i).name '\' Months(j).name ]);
        for z = 3 : length(Instruments)
            Content = dir([ InputParent Exchanges(i).name ...
                '\' Months(j).name '\' Instruments(z).name '\shfe_*.csv' ]);
            for k = 1 : length(Content)
                Files = [ Files; [ Exchanges(i).name '\' Months(j).name ...
                    '\' Instruments(z).name '\' Content(k).name ] ];
            end
        end
    end
end

% start loop
parfor i = 1 : size(Files, 1)
    
    InputFile = Files(i, :);
    Symbol = lower(InputFile(find(InputFile == '_', 1, 'last') + 1 : find(InputFile == '.', 1, 'first') - 5));
    TradingDay = datetime(InputFile(find(InputFile == '_', 1, 'first') + 1 : find(InputFile == '_', 1, 'last') - 1), 'InputFormat', 'yyyyMMdd');
    
    % open and scan csv file
    Data = readtable([ InputParent InputFile ]);
    
    % convert datetime
    Time = datetime(Data.UpdateTime, 'InputFormat', 'HH:mm:ss');
    PrevDayIdx = Time.Hour > 16;
    Time(PrevDayIdx) = Time(PrevDayIdx) - 1;
    TimeOffset = Time - datetime('today') + 1;
    
    SecondOfDay = int64(seconds(TimeOffset));
    UpdateMillisec = int64(Data.UpdateMillisec);
    LastPrice = double(Data.LastPrice);
    AskPrice1 = double(Data.AskPrice1);
    AskVolume1 = int64(Data.AskVolume1);
    BidPrice1 = double(Data.BidPrice1);
    BidVolume1 = int64(Data.BidVolume1);
    OpenPrice = double(Data.OpenPrice);
    ClosePrice = double(Data.ClosePrice);
    UpperLimitPrice = double(Data.UpperLimitPrice);
    LowerLimitPrice = double(Data.LowerLimitPrice);
    HighestPrice = double(Data.HighestPrice);
    LowestPrice = double(Data.LowestPrice);
    OpenInterest = int64(Data.OpenInterest);
    Volume = int64(Data.Volume);
    Turnover = double(Data.Turnover);

    OutputData = struct(...
        'SecondOfDay', SecondOfDay, ...
        'UpdateMillisec', UpdateMillisec, ...
        'LastPrice', LastPrice, ...
        'AskPrice1', AskPrice1, ...
        'AskVolume1', AskVolume1, ...
        'BidPrice1', BidPrice1, ...
        'BidVolume1', BidVolume1, ...
        'OpenPrice', OpenPrice, ...
        'ClosePrice', ClosePrice, ...
        'UpperLimitPrice', UpperLimitPrice, ...
        'LowerLimitPrice', LowerLimitPrice, ...
        'HighestPrice', HighestPrice, ...
        'LowestPrice', LowestPrice, ...
        'OpenInterest', OpenInterest, ...
        'Turnover', Turnover, ...
        'Volume', Volume);

    OutputDir = [ OutputParent 'shfe\' datestr(TradingDay, 'yyyymm') '\' Symbol '\' ];
    OutputFile = [ InputFile(1 : end - 4) '.mat' ];
    
    if ~exist(OutputDir, 'dir')
        mkdir(OutputDir);
    end
    StructParSave([ OutputParent OutputFile ], OutputData);
    
end