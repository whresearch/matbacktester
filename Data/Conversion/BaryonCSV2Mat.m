% This script converts all .mat files to uniform data with two snapshots
% per second and following SHFE trading hours

% clear workspace
clear;

% list all files
InputParent = 'C:\Users\Ranye Lu\Desktop\20151215\';
OutputParent = 'C:\Users\Ranye Lu\Desktop\20151215\Mat';
Files = dir([ InputParent '*.csv' ]);

% start loop
parfor i = 1 : length(Files)
    
    InputFile = Files(i).name;
    Symbol = lower(InputFile(1 : find(InputFile == '_', 1, 'first') - 5));
    TradingDay = datetime(InputFile(find(InputFile == '_', 1, 'first') + 1 : find(InputFile == '.', 1, 'first') - 1), 'InputFormat', 'yyyyMMdd');
    
    if strcmp(Symbol, 'cu') || strcmp(Symbol, 'al') || strcmp(Symbol, 'zn') ...
    || strcmp(Symbol, 'pb') || strcmp(Symbol, 'ni') || strcmp(Symbol, 'sn') ...
    || strcmp(Symbol, 'ru') || strcmp(Symbol, 'au') || strcmp(Symbol, 'ag') ...
    || strcmp(Symbol, 'rb') || strcmp(Symbol, 'wr') || strcmp(Symbol, 'hc') ...
    || strcmp(Symbol, 'fu') || strcmp(Symbol, 'bu')

        % open and scan csv file
        data = readtable([ InputParent InputFile ]);

        % convert datetime
        dt = datetime(data.TradingOfficeTime, 'InputFormat', 'yyyyMMdd HH:mm:ss.SSS');
        SecOffset = seconds(dt - TradingDay);
        SecondOfDay = floor(SecOffset);
        UpdateMillisec = (SecOffset - SecondOfDay) * 1000;

        LastPrice = double(data.LastPrice);
        AskPrice1 = double(data.AskPrice1);
        AskVolume1 = int64(data.AskVolume1);
        BidPrice1 = double(data.BidPrice1);
        BidVolume1 = int64(data.BidVolume1);
        OpenPrice = double(data.OpenPrice);
        ClosePrice = NaN(length(data.OpenPrice), 1);
        UpperLimitPrice = double(data.UpperLimitPrice);
        LowerLimitPrice = double(data.LowerLimitPrice);
        HighestPrice = double(data.HighestPrice);
        LowestPrice = double(data.LowestPrice);
        OpenInterest = int64(data.OpenInterest);
        OpenInterestAbs = cumsum(OpenInterest);
        Turnover = cumsum(int64(data.Turnover));
        Volume = int64(data.Volume);
        VolumeAbs = cumsum(Volume);

        OutputData = struct(...
            'SecondOfDay', SecondOfDay, ...
            'UpdateMillisec', UpdateMillisec, ...
            'LastPrice', LastPrice, ...
            'AskPrice1', AskPrice1, ...
            'AskVolume1', AskVolume1, ...
            'BidPrice1', BidPrice1, ...
            'BidVolume1', BidVolume1, ...
            'OpenPrice', OpenPrice, ...
            'ClosePrice', ClosePrice, ...
            'UpperLimitPrice', UpperLimitPrice, ...
            'LowerLimitPrice', LowerLimitPrice, ...
            'HighestPrice', HighestPrice, ...
            'LowestPrice', LowestPrice, ...
            'OpenInterest', OpenInterest, ...
            'OpenInterestAbs', OpenInterestAbs, ...
            'Turnover', Turnover, ...
            'Volume', Volume, ...
            'VolumeAbs', VolumeAbs);
        
        OutputDir = [ OutputParent 'shfe\' datestr(TradingDay, 'yyyymm') '\' Symbol '\' ];
        OutputFile = [ 'shfe_' InputFile(8 : 15) '_' InputFile(1 : 6) '.mat' ];
        
        if ~exist(OutputDir, 'dir')
            mkdir(OutputDir);
        end
        StructParSave([ OutputDir OutputFile ], OutputData);
    end
    
end