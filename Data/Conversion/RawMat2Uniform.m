% This script converts all .mat files to uniform data with two snapshots
% per second and following SHFE trading hours

% clear workspace
clear;

% list all files
InputParent = 'D:\Data\MatData_Raw\';
OutputParent = 'D:\Data\MatData_Uniform\';

Files = [];
Exchanges = dir(InputParent);
for i = 3 : length(Exchanges)
    Months = dir([ InputParent Exchanges(i).name ]);
    for j = 3 : length(Months)
        Instruments = dir([ InputParent Exchanges(i).name '\' Months(j).name ]);
        for z = 3 : length(Instruments)
            Content = dir([ InputParent Exchanges(i).name ...
                '\' Months(j).name '\' Instruments(z).name '\shfe_*.mat' ]);
            for k = 1 : length(Content)
                Files = [ Files; [ Exchanges(i).name '\' Months(j).name ...
                    '\' Instruments(z).name '\' Content(k).name ] ];
            end
        end
    end
end

% start loop
parfor i = 1 : size(Files, 1)
    
    InputFile = Files(i, :);
    Symbol = lower(InputFile(find(InputFile == '_', 1, 'last') + 1 : find(InputFile == '.', 1, 'first') - 5));
    TradingDay = datetime(InputFile(find(InputFile == '_', 1, 'first') + 1 : find(InputFile == '_', 1, 'last') - 1), 'InputFormat', 'yyyyMMdd');
    
    Data = load([ InputParent InputFile ]);
    SourceTime = double(Data.SecondOfDay) + double(Data.UpdateMillisec) / 1000;
    
    Contract = GetContractFromSymbol(Symbol);
    TargetTime = [];
    for j = 1 : length(Contract.OpenHour)
        TargetTime = [ TargetTime; transpose(Contract.OpenHour(j) * 3600 : 0.5 : Contract.CloseHour(j) * 3600) ];
    end
    TargetTime = TargetTime + 24 * 3600;
    SecondOfDay = floor(TargetTime);
    UpdateMillisec = (TargetTime - SecondOfDay) * 1000;
    Idx = NaN(length(TargetTime), 1);
    NaNCount = 0;
    
    [ dt, k ] = min(SourceTime);
    j = 1;
    while TargetTime(j) < dt
        NaNCount = NaNCount + 1;
        j = j + 1;
    end
    while j <= length(TargetTime)
        while k < length(SourceTime) && dt < TargetTime(j)
            k = k + 1;
            dt = SourceTime(k);
        end
        Idx(j) = k;
        j = j + 1;
    end
    
    NaNFront = NaN(NaNCount, 1);
    ZeroFront = zeros(NaNCount, 1);
    Idx(1 : NaNCount) = [];
    
    LastPrice = [ NaNFront; double(Data.LastPrice(Idx)) ];
    AskPrice1 = [ NaNFront; double(Data.AskPrice1(Idx)) ];
    AskVolume1 = [ NaNFront; int64(Data.AskVolume1(Idx)) ];
    BidPrice1 = [ NaNFront; double(Data.BidPrice1(Idx)) ];
    BidVolume1 = [ NaNFront; int64(Data.BidVolume1(Idx)) ];
    OpenPrice = [ NaNFront; double(Data.OpenPrice(Idx)) ];
    ClosePrice = [ NaNFront; double(Data.ClosePrice(Idx)) ];
    HighestPrice = [ NaNFront; double(Data.HighestPrice(Idx)) ];
    LowestPrice = [ NaNFront; double(Data.LowestPrice(Idx)) ];
    LowerLimitPrice = [ NaNFront; double(Data.LowerLimitPrice(Idx)) ];
    UpperLimitPrice = [ NaNFront; double(Data.UpperLimitPrice(Idx)) ];
    OpenInterest = [ int64(repmat(Data.OpenInterest(Idx(1)), NaNCount, 1)) ; int64(Data.OpenInterest(Idx)) ];
    Volume = [ ZeroFront; int64(Data.Volume(Idx)) ];
    Turnover = [ ZeroFront; int64(Data.Turnover(Idx)) ];
    
    OutputData = struct(...
        'SecondOfDay', SecondOfDay, ...
        'UpdateMillisec', UpdateMillisec, ...
        'LastPrice', LastPrice, ...
        'AskPrice1', AskPrice1, ...
        'AskVolume1', AskVolume1, ...
        'BidPrice1', BidPrice1, ...
        'BidVolume1', BidVolume1, ...
        'OpenPrice', OpenPrice, ...
        'ClosePrice', ClosePrice, ...
        'HighestPrice', HighestPrice, ...
        'LowestPrice', LowestPrice, ...
        'UpperLimitPrice', UpperLimitPrice, ...
        'LowerLimitPrice', LowerLimitPrice, ...
        'OpenInterest', OpenInterest, ...
        'Turnover', Turnover, ...
        'Volume', Volume);

    OutputDir = [ OutputParent 'shfe\' datestr(TradingDay, 'yyyymm') '\' Symbol '\' ];
    OutputFile = InputFile;
        
    if ~exist(OutputDir, 'dir')
        mkdir(OutputDir);
    end
    StructParSave([ OutputParent OutputFile ], OutputData);
end