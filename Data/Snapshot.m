classdef Snapshot
    
    properties
        Time
        Last
        Bid
        Ask
        Mid
        SWMid
        BidSize
        AskSize
        OpenInterest
        Volume
        OpenVolume
        CloseVolume
        Turnover
        VWAP
    end
    
end

