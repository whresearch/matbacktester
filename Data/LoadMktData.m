function [ MktData ] = LoadMktData( Instrument, StartDate, EndDate )
%LOADMKTDATA Summary of this function goes here
%   Detailed explanation goes here

if nargin < 2; error('2 arguments required'); end
if nargin < 3; EndDate = StartDate; end
            
% create price series
MktData = PriceSeries;
MktData.Init(GetContract(Instrument));

% start loop
Date = StartDate;
while Date <= EndDate
    File = strcat('shfe_', datestr(Date, 'yyyymmdd'), '_', Instrument, '.mat');
    % check if file exists
    if exist(File, 'file')
        MktData.AppendPriceSeries(LoadMat_SHFE(File));
    end
    % move to next date
    Date = Date + 1;
end

end

