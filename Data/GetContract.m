function Output = GetContract( Symbol )
%GetContractDetail Get the detail information of the contract

switch lower(Symbol(1:2))
    case 'cu'   % Copper [ night session close at 1:00 am ]
        Multiplier = 5;
        TickSize = 10;
        MaxDepth = 1;
        Exchange = 'shfe';
        OpenHour = [ -3; 9; 10.5; 13.5 ];
        CloseHour = [ 1; 10.25; 11.5; 15 ];
        OpenCommissionByMoney = 0.5585 / 1e4;
        OpenCommissionByVolume = 0;
        CloseCommissionByMoney = 0;
        CloseCommissionByVolume = 0.15;
    case 'al'   % Aluminum [ night session close at 1:00 am ]
        Multiplier = 5;
        TickSize = 5;
        MaxDepth = 1;
        Exchange = 'shfe';
        OpenHour = [ -3; 9; 10.5; 13.5 ];
        CloseHour = [ 1; 10.25; 11.5; 15 ];
        OpenCommissionByMoney = 8 / 1e8;
        OpenCommissionByVolume = 0;
        CloseCommissionByMoney = 0;
        CloseCommissionByVolume = 0;
    case 'zn'   % Zinc [ night session close at 1:00 am ]
        Multiplier = 5;
        TickSize = 5;
        MaxDepth = 1;
        Exchange = 'shfe';
        OpenHour = [ -3; 9; 10.5; 13.5 ];
        CloseHour = [ 1; 10.25; 11.5; 15 ];
        OpenCommissionByMoney = 8 / 1e8;
        OpenCommissionByVolume = 3.3;
        CloseCommissionByMoney = 8 / 1e8;
        CloseCommissionByVolume = 0;
    case 'pb'   % Lead [ night session close at 1:00 am ]
        Multiplier = 5;
        TickSize = 5;
        MaxDepth = 1;
        Exchange = 'shfe';
        OpenHour = [ -3; 9; 10.5; 13.5 ];
        CloseHour = [ 1; 10.25; 11.5; 15 ];
        OpenCommissionByMoney = 8 / 1e8;
        OpenCommissionByVolume = 0;
        CloseCommissionByMoney = 0;
        CloseCommissionByVolume = 0;
    case 'ni'   % Nickel [ night session close at 1:00 am ]
        Multiplier = 1;
        TickSize = 10;
        MaxDepth = 1;
        Exchange = 'shfe';
        OpenHour = [ -3; 9; 10.5; 13.5 ];
        CloseHour = [ 1; 10.25; 11.5; 15 ];
        OpenCommissionByMoney = 8 / 1e8;
        OpenCommissionByVolume = 6.6;
        CloseCommissionByMoney = 0;
        CloseCommissionByVolume = 6.6;
    case 'sn'   % Stannum [ night session close at 1:00 am ]
        Multiplier = 1;
        TickSize = 10;
        MaxDepth = 1;
        Exchange = 'shfe';
        OpenHour = [ -3; 9; 10.5; 13.5 ];
        CloseHour = [ 1; 10.25; 11.5; 15 ];
        OpenCommissionByMoney = 8 / 1e8;
        OpenCommissionByVolume = 0;
        CloseCommissionByMoney = 0;
        CloseCommissionByVolume = 0;
    case 'ru'   % Natural Rubber [ night session close at 11:00 pm T-1 ]
        Multiplier = 10;
        TickSize = 5;
        MaxDepth = 1;
        Exchange = 'shfe';
        OpenHour = [ -3; 9; 10.5; 13.5 ];
        CloseHour = [ -1; 10.25; 11.5; 15 ];
        OpenCommissionByMoney = 8 / 1e8;
        OpenCommissionByVolume = 0;
        CloseCommissionByMoney = 0;
        CloseCommissionByVolume = 0;
    case 'au'   % Gold [ night session close at 2:30 am ]
        Multiplier = 1000;
        TickSize = 0.05;
        MaxDepth = 1;
        Exchange = 'shfe';
        OpenHour = [ -3; 9; 10.5; 13.5 ];
        CloseHour = [ 2.5; 10.25; 11.5; 15 ];
        OpenCommissionByMoney = 8 / 1e8;
        OpenCommissionByVolume = 0;
        CloseCommissionByMoney = 0;
        CloseCommissionByVolume = 0;
    case 'ag'   % Silver [ night session close at 2:30 am ]
        Multiplier = 15;
        TickSize = 1;
        MaxDepth = 1;
        Exchange = 'shfe';
        OpenHour = [ -3; 9; 10.5; 13.5 ];
        CloseHour = [ 2.5; 10.25; 11.5; 15 ];
        OpenCommissionByMoney = 8 / 1e8;
        OpenCommissionByVolume = 0;
        CloseCommissionByMoney = 0;
        CloseCommissionByVolume = 0;
    case 'rb'   % Steel Rebar [ night session close at 1:00 am ]
        Multiplier = 10;
        TickSize = 1;
        MaxDepth = 1;
        Exchange = 'shfe';
        OpenHour = [ -3; 9; 10.5; 13.5 ];
        CloseHour = [ 1; 10.25; 11.5; 15 ];
        OpenCommissionByMoney = 8 / 1e8;
        OpenCommissionByVolume = 0;
        CloseCommissionByMoney = 0;
        CloseCommissionByVolume = 0;
    case 'wr'   % Wire Rod [ night session close at 1:00 am ]
        Multiplier = 10;
        TickSize = 1;
        MaxDepth = 1;
        Exchange = 'shfe';
        OpenHour = [ -3; 9; 10.5; 13.5 ];
        CloseHour = [ 1; 10.25; 11.5; 15 ];
        OpenCommissionByMoney = 8 / 1e8;
        OpenCommissionByVolume = 0;
        CloseCommissionByMoney = 0;
        CloseCommissionByVolume = 0;
    case 'hc'   % Hot-rolled Coil [ night session close at 1:00 am ]
        Multiplier = 10;
        TickSize = 1;
        MaxDepth = 1;
        Exchange = 'shfe';
        OpenHour = [ -3; 9; 10.5; 13.5 ];
        CloseHour = [ 1; 10.25; 11.5; 15 ];
        OpenCommissionByMoney = 8 / 1e8;
        OpenCommissionByVolume = 0;
        CloseCommissionByMoney = 0;
        CloseCommissionByVolume = 0;
    case 'fu'   % Fuel Oil [ night session close at 1:00 am ]
        Multiplier = 50;
        TickSize = 1;
        MaxDepth = 1;
        Exchange = 'shfe';
        OpenHour = [ -3; 9; 10.5; 13.5 ];
        CloseHour = [ 1; 10.25; 11.5; 15 ];
        OpenCommissionByMoney = 8 / 1e8;
        OpenCommissionByVolume = 0;
        CloseCommissionByMoney = 0;
        CloseCommissionByVolume = 0;
    case 'bu'   % Bitumen [ night session close at 1:00 am ]
        Multiplier = 10;
        TickSize = 2;
        MaxDepth = 1;
        Exchange = 'shfe';
        OpenHour = [ -3; 9; 10.5; 13.5 ];
        CloseHour = [ 1; 10.25; 11.5; 15 ];
        OpenCommissionByMoney = 8 / 1e8;
        OpenCommissionByVolume = 0;
        CloseCommissionByMoney = 0;
        CloseCommissionByVolume = 0;
    otherwise
        error('unknown contract');
end

Output = Contract(Symbol, Exchange, Multiplier, TickSize, MaxDepth, ...
    OpenHour, CloseHour, OpenCommissionByMoney, OpenCommissionByVolume, ...
    CloseCommissionByMoney, CloseCommissionByVolume);

end

