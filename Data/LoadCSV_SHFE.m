function [ Output ] = LoadCSV_SHFE( File )
%LoadCSV_SHFE loads a single CSV file using SHFE settings

% open and scan csv file
fileId = fopen(File);
fscanf(fileId, '%s %s %s %f %f %s %f %f %s %s %s %s %f %f %f %f');
data = textscan(fileId, '%s %s %s %f %f %s %f %f %s %s %s %s %f %f %f %f','Delimiter',',');

% create price series
Contract = GetContract(data{1, 2}(1));
Output = PriceSeries;
Output.Init(Contract);

% convert datetime
tm = datetime(data{1, 3}, 'InputFormat', 'yyyy-MM-dd HH:mm:ss.SSS');
tm.Format = 'yyyy-MM-dd HH:mm:ss.SSS';
[y, m, d] = ymd(tm(end));
day = datetime(y, m, d);

% get open price, upper and lower limit prices
open;
upperlimit;
lowerlimit;

% calculate open interest
oi = data{1, 5};
oi(2 : end) = oi(2 : end) - oi(1 : end - 1);
oi(1) = 0;

% calculate volume
volume = data{1, 8};
volume(2 : end) = volume(2 : end) - volume(1 : end - 1);

% calcualte turnover
turnover = data{1, 7};
turnover(2 : end) = turnover(2 : end) - turnover(1 : end - 1);

% append data
Output.Append(...
    day, ...            % day
    open, ...           % day open price
    upperlimit, ...     % day upper limit price
    lowerlimit, ...     % day lower limit price
    tm, ...             % time
    data{1, 4}, ...     % last
    data{1, 13}, ...    % bid
    data{1, 14}, ...    % ask
    data{1, 15}, ...    % bid size
    data{1, 16}, ...    % ask size
    oi, ...             % open interest
    volume, ...         % volume
    turnover);          % turnover

end

