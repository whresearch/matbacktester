function [ Price, Amount, IsLong ] = SplitVWAP( CurrMktData, PrevMktData, Mode )
%GETVWAPORDERS Summary of this function goes here
%   Detailed explanation goes here
% VWAP Modes: 
%   0 - Four Levels: Closet four price levels around VWAP
%   1 - Two Levels: Bid/Ask of current tick
%   2 - Two Levels: Bid/Ask of previous tick
%   3 - One Level: VWAP only

Price = [];
Amount = [];
IsLong = [];

VWAP = CurrMktData.VWAP;
Volume = CurrMktData.Volume / 2;
        
switch Mode
    case 0 % Four Levels: Closet four price levels around VWAP

        Bid = CurrMktData.Bid(1);
        Ask = CurrMktData.Ask(1);
        TradePrice = NaN(4, 1);
        TradeVolume = NaN(4, 1);

        if Ask - Bid >= 3 * this.TickSize
            W1 = round(0.1 * Volume);
            W2 = Volume - W1;
        else
            W1 = 0;
            W2 = Volume;
        end
        if mod(VWAP, this.TickSize) == 0
            if VWAP > this.PrevMktData.Mid
                TradePrice(2) = VWAP - this.TickSize;
                TradePrice(3) = VWAP;
            else
                TradePrice(2) = VWAP;
                TradePrice(3) = VWAP + this.TickSize;
            end
        else
            TradePrice(2) = floor(VWAP / this.TickSize) * this.TickSize;
            TradePrice(3) = TradePrice(2) + this.TickSize;
        end

        TradePrice(1) = TradePrice(2) - this.TickSize;
        TradePrice(4) = TradePrice(3) + this.TickSize;
        TradeVolume(4) = round(W1 * (VWAP - TradePrice(1)) / (TradePrice(4) - TradePrice(1)));
        TradeVolume(3) = round(W2 * (VWAP - TradePrice(2)) / (TradePrice(3) - TradePrice(2)));
        TradeVolume(2) = W2 - TradeVolume(3);
        TradeVolume(1) = W1 - TradeVolume(4);

        PrevMid = this.PrevMktData.Mid;
        BuyIdx = find(TradePrice >= PrevMid);
        SellIdx = flip(find(TradePrice < PrevMid));

        for i = 1 : length(BuyIdx);
            if TradeVolume(BuyIdx(i)) > 0
                Price = [ Price; TradePrice(BuyIdx(i)) ];
                Amount = [ Amount; TradeVolume(BuyIdx(i)) ];
                IsLong = [ IsLong; true ];
            end
        end
        for i = 1 : length(SellIdx);
            if TradeVolume(SellIdx(i)) > 0
                Price = [ Price; TradePrice(SellIdx(i)) ];
                Amount = [ Amount; TradeVolume(SellIdx(i)) ];
                IsLong = [ IsLong; false ];
            end
        end

    case 1 % Two Levels: Bid/Ask of current tick

        Bid = CurrMktData.Bid(1);
        Ask = CurrMktData.Ask(1);

        if VWAP >= Ask
            AskTradeSize = Volume;
            BidTradeSize = 0;
        elseif VWAP <= Bid
            AskTradeSize = 0;
            BidTradeSize = Volume;
        else
            AskTradeSize = round(Volume * (VWAP - Bid) / (Ask - Bid));
            BidTradeSize = Volume - AskTradeSize;
        end

        if AskTradeSize > 0
            Price = [ Price; Ask ];
            Amount = [ Amount; AskTradeSize ];
            IsLong = [ IsLong; true ];
        end
        if BidTradeSize > 0
            Price = [ Price; Bid ];
            Amount = [ Amount; BidTradeSize ];
            IsLong = [ IsLong; false ];
        end

    case 2 % Two Levels: Bid/Ask of previous tick

        Bid = PrevMktData.Bid(1);
        Ask = PrevMktData.Ask(1);

        if VWAP >= Ask
            AskTradeSize = Volume;
            BidTradeSize = 0;
        elseif VWAP <= Bid
            AskTradeSize = 0;
            BidTradeSize = Volume;
        else
            AskTradeSize = round(Volume * (VWAP - Bid) / (Ask - Bid));
            BidTradeSize = Volume - AskTradeSize;
        end

        if AskTradeSize > 0
            Price = [ Price; Ask ];
            Amount = [ Amount; AskTradeSize ];
            IsLong = [ IsLong; true ];
        end
        if BidTradeSize > 0
            Price = [ Price; Bid ];
            Amount = [ Amount; BidTradeSize ];
            IsLong = [ IsLong; false ];
        end

    case 3 % One Level: VWAP only
        
        Price = [ Price; VWAP ];
        Amount = [ Amount; Volume ];
        if VWAP >= PrevMktData.Mid
            IsLong = [ IsLong; true ];
        else
            IsLong = [ IsLong; false ];
        end

    otherwise
        error('unknown mode');
end

end

